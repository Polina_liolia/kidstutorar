namespace KidsTutorAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LocalizationSupportMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Words",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Locale = c.String(),
                        Text = c.String(),
                        ComplexityLevel = c.Int(nullable: false),
                        Bundle_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bundles", t => t.Bundle_Id, cascadeDelete: true)
                .Index(t => t.Bundle_Id);
            
            DropColumn("dbo.Bundles", "Word");
            DropColumn("dbo.Bundles", "ComplexityLevel");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Bundles", "ComplexityLevel", c => c.Int(nullable: false));
            AddColumn("dbo.Bundles", "Word", c => c.String(nullable: false));
            DropForeignKey("dbo.Words", "Bundle_Id", "dbo.Bundles");
            DropIndex("dbo.Words", new[] { "Bundle_Id" });
            DropTable("dbo.Words");
        }
    }
}
