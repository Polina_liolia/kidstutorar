namespace KidsTutorAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RewardsAdded : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GaleryItems", "Picture_Id", "dbo.Pictures");
            DropForeignKey("dbo.GaleryItems", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.GaleryItems", new[] { "Picture_Id" });
            DropIndex("dbo.GaleryItems", new[] { "User_Id" });
            CreateTable(
                "dbo.Rewards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Url = c.String(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            DropTable("dbo.GaleryItems");
            DropTable("dbo.Pictures");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Path = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GaleryItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PartsCounter = c.Short(nullable: false),
                        Picture_Id = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Rewards", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Rewards", new[] { "ApplicationUser_Id" });
            DropTable("dbo.Rewards");
            CreateIndex("dbo.GaleryItems", "User_Id");
            CreateIndex("dbo.GaleryItems", "Picture_Id");
            AddForeignKey("dbo.GaleryItems", "User_Id", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.GaleryItems", "Picture_Id", "dbo.Pictures", "Id", cascadeDelete: true);
        }
    }
}
