﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadAssetBundlesScript : MonoBehaviour {

    public string BundleURL;
    public string AssetName;
    public int version;
    public Transform Parent;

    void Start()
    {
        StartCoroutine(DownloadAndCache());
    }

    IEnumerator DownloadAndCache()
    {
        // Wait for the Caching system to be ready
        while (!Caching.ready)
            yield return null;

        // Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
        using (WWW www = WWW.LoadFromCacheOrDownload(BundleURL, version))
        {
            yield return www;
            if (www.error != null)
                throw new System.Exception("WWW download had an error:" + www.error);
            AssetBundle bundle = www.assetBundle;
            Debug.Log(bundle);
            Object obj = null;
            if (AssetName == "")
            {
                obj = bundle.mainAsset;
                Instantiate(obj);
            }
                
            else
            {
                obj = bundle.LoadAsset(AssetName);
                if (obj is GameObject)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        GameObject gameObject = obj as GameObject;
                        if (gameObject != null)
                        {
                            gameObject.transform.localScale = new Vector3(0.01F, 0.01F, 0.01F);
                            gameObject.transform.position += new Vector3(i, 0, i);
                            gameObject.transform.SetParent(Parent, false);
                            Instantiate(gameObject);
                            Debug.LogWarning("GAME OBJECT!");
                        }
                        else
                        {
                            Instantiate(obj);
                            Debug.LogWarning("OBJECT!");
                        }
                    }
                    
                }
               
               
            }
               
            // Unload the AssetBundles compressed contents to conserve memory
            bundle.Unload(false);

        } // memory is freed from the web stream (www.Dispose() gets called implicitly)
    }
}
