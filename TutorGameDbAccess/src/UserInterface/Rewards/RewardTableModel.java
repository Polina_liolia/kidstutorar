package UserInterface.Rewards;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import tutorgamedbaccess.Reward;

public class RewardTableModel extends AbstractTableModel {
 
    List<Reward> rewards;

    //columns definition
    String[] columns = new String[]{
       "Id", "Picture name", "Reward path", "Count"
    };
    private Class[] columnsTypes = new Class[]{
        Long.class, String.class, String.class, Integer.class
    };

    public RewardTableModel(List<Reward> rewards) {
        this.rewards = rewards;
    }
    
    @Override
    public int getRowCount() {
        return rewards.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Reward currentReward = rewards.get(rowIndex);
        return 0 == columnIndex ? currentReward.getId(): 
               1 == columnIndex ? currentReward.getName():
               2 == columnIndex ? currentReward.getUrl(): 
               3 == columnIndex ? currentReward.getCount() : null;
    }
    @Override
    public String getColumnName(int column) {
        return columns[column];
    }
    
    @Override
    public Class<?> getColumnClass(int column)
    {
        return columnsTypes[column];
    }

    public Reward getReward(int row) {
        return rewards.get(row);
    }
}
