﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace KidsTutorAPI.Models
{
    [DataContract]
    [Serializable]
    public class Bundle
    {
        [Key]
        [DataMember]
        public int Id { get; set; }

        [Required]
        [DataMember]
        public string Name { get; set; }

        [Required]
        [DataMember]
        public string Path { get; set; }

        [Required]
        [DataMember]
        public string AssetName { get; set; }

        [DataMember]
        public bool IsAction { get; set; }

        [DataMember]
        public virtual IList<Category> Categories { get; set; }

        [Required]
        [DataMember]
        public virtual IList<Word> Words { get; set; }

        [DataMember]
        public bool IsFree { get; set; }

        public Bundle()
        {
            Categories = new List<Category>();
            Words = new List<Word>();
            IsFree = true;
        }
        public Bundle(string name, string path, string assetName, bool isAction = false, bool isFree=true)
        {
            Words = new List<Word>();
            Categories = new List<Category>();
            Name = name;
            Path = path;
            AssetName = assetName;
            IsAction = isAction;
            IsFree = isFree;
        }
    }
}
