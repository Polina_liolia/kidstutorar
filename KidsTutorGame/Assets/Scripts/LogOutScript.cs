﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogOutScript : MonoBehaviour
{

    public void OnLogOutClick()
    {
        ServerClientScript.UserToken = null;
        ServerClientScript.IsSignedIn = false;
        ServerClientScript.User = new ApplicationUser() { ComplexityLevel = 1 };
        GameDataController.User = ServerClientScript.User;
        GameDataController.AuthData = new AuthData();
        SceneManager.LoadScene(sceneName: "MenuScene");
    }
}
