﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;

using System.Net;
using System.Collections;
using UnityEngine.Networking;

namespace SpeechToTextLib
{
    public class SpeechToTextConverter
    {
        public AudioSource GoAudioSource { get; set; }
        public MonoBehaviour App { get; set; }
        public bool IsMicConnected { get; set; }
        public string Language { get; set; }

        private int minFreq, maxFreq;
        private string apiKey;

        public SpeechToTextConverter(MonoBehaviour app, string apiKey, string language= "ru-RU")
        {
            App = app;
            this.apiKey = apiKey;
            Language = language;
        }

        public bool CheckMicrophone()
        {
            //Check if there is at least one microphone connected
            if (Microphone.devices.Length <= 0)
            {
                //Throw a warning message at the console if there isn't
                Debug.LogWarning("Microphone not connected!");
            }
            else //At least one microphone is present
            {
                //Set 'micConnected' to true
                IsMicConnected = true;

                
                //Get the default microphone recording capabilities
                Microphone.GetDeviceCaps(null, out minFreq, out maxFreq);

                //According to the documentation, if minFreq and maxFreq are zero, the microphone supports any frequency...
                if (minFreq == 0 && maxFreq == 0)
                {
                    //...meaning 44100 Hz can be used as the recording sampling rate
                    maxFreq = 44100;
                }

                //Get the attached AudioSource component
                GoAudioSource = App.GetComponent<AudioSource>();
                if (GoAudioSource == null)
                    Debug.Log("No audio sourse");
            }
            return IsMicConnected;
        }

        public void StartRecording(int duration = 7)
        {
            //If there is a microphone
            if (IsMicConnected)
            {
                //If the audio from any microphone isn't being recorded
                if (!Microphone.IsRecording(null))
                {
                        //Start recording and store the audio captured from the microphone at the AudioClip in the AudioSource
                        GoAudioSource.clip = Microphone.Start(null, true, duration, maxFreq); //default set for a 7 second clip
                }

            }
            else // No microphone
            {
                Debug.Log("Microphone not connected!");
            }
        }

        public IEnumerator ProcessRecord(Action<string> getWord)
        {
            string resultString = "";
            float filenameRand = UnityEngine.Random.Range(0.0f, 10.0f);
            string filename = "speech_record" + filenameRand;
            Microphone.End(null); //Stop the audio recording
            Debug.Log("Recording Stopped");

            if (!filename.ToLower().EndsWith(".wav"))
            {
                filename += ".wav";
            }

            var filePath = Path.Combine("records/", filename);
            filePath = Path.Combine(Application.persistentDataPath, filePath);
            Debug.Log("Created filepath string: " + filePath);

            // Make sure directory exists if user is saving to sub dir.
            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            SavWav.Save(filePath, GoAudioSource.clip); //Save a temporary Wav File
            Debug.Log("Saving @ " + filePath);
            //Insert your API KEY here.
            string apiURL = "https://speech.googleapis.com/v1/speech:recognize?&key=" + this.apiKey;
            string Response = "";

            Debug.Log("Uploading " + filePath);

            yield return App.StartCoroutine(HttpUploadFile(apiURL, filePath, "file", "audio/wav; rate=44100", (resp) => Response = resp ));
            Debug.Log("Response String: " + Response);

            if (Response.Equals("{}"))
            {
                getWord("error");
                yield break;
            }

            var jsonresponse = SimpleJSON.JSON.Parse(Response);

            if (jsonresponse != null && jsonresponse.Keys.Contains("results"))
            {
                string json  = jsonresponse["results"][0].ToString();
                //Debug.Log("JSON  " + json);

                var jsonResults = SimpleJSON.JSON.Parse(json);
                
                resultString = jsonResults["alternatives"][0]["transcript"].ToString();
              //  Debug.Log("resultString   " + resultString);

                //remove brackets:
                resultString = resultString.TrimStart('"');
                resultString = resultString.TrimEnd('"');
              
               // TextBox.text = transcripts;

            }
            else
            {
                resultString = "error";
            }
            //goAudioSource.Play(); //Playback the recorded audio
            File.Delete(filePath); //Delete the Temporary Wav file
            getWord(resultString); 
        }

        public IEnumerator HttpUploadFile(string url, string file, string paramName, string contentType, Action<string> getResponse)
        {
            if (!File.Exists(file))
            {
                getResponse("error");
                yield break;
            }
            //try
            //{
                string result = "";
                System.Net.ServicePointManager.ServerCertificateValidationCallback += (o, certificate, chain, errors) => true;
                Debug.Log(string.Format("Uploading {0} to {1}", file, url));
                Byte[] bytes = File.ReadAllBytes(file);
                string file64 = Convert.ToBase64String(bytes, Base64FormattingOptions.None);
                Debug.Log(file64);
            
                //var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                //httpWebRequest.ContentType = "application/json";
                //httpWebRequest.Method = "POST";
                //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                //{
                //    string json = "{ \"config\": { \"languageCode\" : \"" + Language + "\" }, \"audio\" : { \"content\" : \"" + file64 + "\"}}";

                //   // Debug.Log(json);
                //    streamWriter.Write(json);
                //    streamWriter.Flush();
                //    streamWriter.Close();
                //}

                //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //Debug.Log(httpResponse);
                //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                //{
                //    result = streamReader.ReadToEnd();
                //    Debug.Log("Response:" + result);
                //}
                //getResponse(result);


                //////////
               
               
                string json = "{ \"config\": { \"languageCode\" : \"" + Language + "\" }, \"audio\" : { \"content\" : \"" + file64 + "\"}}";

                UnityWebRequest www = UnityWebRequest.Post(url, json);

            byte[] bytesJson = Encoding.UTF8.GetBytes(json);
            UploadHandlerRaw uH = new UploadHandlerRaw(bytesJson);
            www.uploadHandler = uH;

            www.SetRequestHeader("Content-Type", "application/json");
            yield return www.SendWebRequest();

            try { 
                if (www.isNetworkError)
                {
                    result = "error";
                    string errorInfo = $"{www.error} error code: {www.responseCode}";
                    Debug.Log(errorInfo);
                }
                else
                {
                    result = www.downloadHandler.text;
                }
                getResponse(result);
            }
            catch (NullReferenceException ex)
            {
                Debug.Log("Speech to text lib, HttpUploadFile: Null refference exception catched");
                getResponse("error");
            }
            catch (WebException ex)
            {
                getResponse("error");
                Debug.Log("Speech to text lib, HttpUploadFile: WebException catched");
            }  
        }
    }
}
