﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LocalizationScript : MonoBehaviour {

    public GameObject[] transtatableObjects; 

    private WordLocalizationData[] localeData = null;
   
     
    void Start () { 
        localeData = LoadJson("localization");
        Debug.Log(localeData);
        if(ServerClientScript.User == null)
        {
            ServerClientScript.User = GameDataController.User;
        }
        string locale = ServerClientScript.User != null && ServerClientScript.User.Locale != null && ServerClientScript.User.Locale != string.Empty ?
           ServerClientScript.User.Locale : Application.systemLanguage == SystemLanguage.Russian ?
           "ru-RU" : "en-US";
        Localize(locale);

    }

    public WordLocalizationData[] LoadJson(string path)
    {
        TextAsset textFile = Resources.Load<TextAsset>(path);

        LocaleJsonRoot jsonData = JsonUtility.FromJson<LocaleJsonRoot>(textFile.text); ;

        return jsonData.root;
    }

    private void Localize(string locale)
    {
        for(int i = 0; i < transtatableObjects.Length; i++ ) //iterating over game objects needed to translate
        {
            Text txt = transtatableObjects[i].GetComponentInChildren<Text>();
            if(txt != null)
            {
                string key = txt.text.ToLower();
                Translation[] translations = null;
                for(int j = 0; j < localeData.Length; j++)
                {
                    if (localeData[j].key.ToLower() == key)
                    {
                        translations = localeData[j].translations;
                        break;
                    }
                }
                //localeData.TryGetValue(key, out translations);
                if (translations != null)
                {
                    for(int j = 0; j < translations.Length; j++) //iterating over translations
                    {
                        if (translations[j].locale.Equals(locale))
                        {
                            txt.text = translations[j].value;
                            break;
                        }
                    }
                }
            }
            
        }
    }
}
