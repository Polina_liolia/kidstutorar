﻿using System;
using System.Collections;
using ServerClientLib;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGameScript : MonoBehaviour
{
    public Text txtHello;
    public GameObject PremiumPopup;
    public GameObject PremiumBtn;
    public GameObject PremiumStar;
    public static int GamesCounter = 0;
    private string locale;

    private void Start()
    {
        PremiumPopup.SetActive(false);
        PremiumStar.SetActive(false);
        if (ServerClientScript.User == null)
        {
            ServerClientScript.User = GameDataController.User;
        }
        locale = ServerClientScript.User != null && ServerClientScript.User.Locale != null && ServerClientScript.User.Locale != string.Empty ?
           ServerClientScript.User.Locale : Application.systemLanguage == SystemLanguage.Russian ?
           "ru-RU" : "en-US";
        if (!ServerClientScript.IsSignedIn)
        {
            StartCoroutine(LogIn());
        }
    }

    private void Update()
    {
        if (txtHello != null && ServerClientScript.User != null && ServerClientScript.User.Name != null && ServerClientScript.User.Name != string.Empty)
        {
            string hello = locale.Equals("ru-RU") ? "Привет" : "Hello";
            txtHello.text =  $"{hello}, {ServerClientScript.User.Name}!";
        }

        if(PremiumBtn != null && ServerClientScript.User != null && ServerClientScript.User.IsPremium)
        {
            PremiumBtn.SetActive(false);
            PremiumStar.SetActive(true);
        }
    }

    public IEnumerator LogIn()
    {
        Debug.Log("Log In action...");
        AuthData authData = GameDataController.AuthData;

        if (authData != null && authData.Token != null && !authData.Token.Equals(string.Empty))
        {
            Debug.Log($"Auth data: {authData}");
            ServerClientScript.UserToken = authData.Token;
            ServerClientScript.IsSignedIn = true;
            ServerClientScript.User = null;
            yield return StartCoroutine(ServerClientScript.GetUser());
            if (ServerClientScript.User.Id == null) //auth with token failed
            {
                yield return ServerClientScript.GetToken(authData.Login, authData.Password);
                if (ServerClientScript.IsSignedIn && ServerClientScript.UserToken != null && !ServerClientScript.UserToken.Equals(string.Empty)) //succeeded
                {
                    yield return StartCoroutine(ServerClientScript.GetUser());
                    if (ServerClientScript.User != null) //succeeded
                    {
                        if (txtHello != null)
                        {
                            string hello = locale.Equals("ru-RU") ? "Привет" : "Hello";
                            txtHello.text = $"{hello}, {ServerClientScript.User.Name}!";
                        }
                    }
                    else //failed
                    {
                        ServerClientScript.IsSignedIn = false;
                        ServerClientScript.User = GameDataController.User;
                    }
                }
                else //failed
                {
                    Debug.Log("Can't get token");
                    ServerClientScript.IsSignedIn = false;
                    ServerClientScript.User = GameDataController.User;
                }
            }
        }
        else
        {
            ServerClientScript.IsSignedIn = false;
            ServerClientScript.User = GameDataController.User;
        }
    }
}
