﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PremiumBtnScript : MonoBehaviour {

    public GameObject PremiumPopup;

	public void OnPremiumBtnClick()
    {
        PremiumPopup.SetActive(true);
    }
}
