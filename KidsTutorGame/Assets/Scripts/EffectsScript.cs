﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsScript : MonoBehaviour
{

    public GameObject preModelEffect;
    public GameObject hideMysteryBoxEffect;
    public GameObject loseEffect;
    public List<GameObject> successEffects;



    public GameObject ShowPreModelEffect(Transform parent, Vector3 position)
    {
        GameObject effect = null;
        if (preModelEffect != null)
        {
            Debug.Log("ShowPreModelEffect");
            effect = Instantiate(preModelEffect);
            effect.transform.SetParent(parent);
            effect.transform.position = position;
        }
        return effect;
    }

    public GameObject ShowHideMysteryBoxEffect(Transform parent, Vector3 position)
    {
        GameObject effect = null;
        if (hideMysteryBoxEffect != null)
        {
            Debug.Log("ShowHideMysteryBoxEffect");
            effect = Instantiate(hideMysteryBoxEffect);
            effect.transform.SetParent(parent);
            effect.transform.position = position;
        }
        return effect;
    }

    public GameObject ShowSuccessEffect(Transform parent, Vector3 position)
    {
        GameObject effect = null;
        if (successEffects != null && successEffects.Count > 0)
        {
            Debug.Log("ShowSuccessEffect");
            int index = Mathf.RoundToInt(Random.Range(0, successEffects.Count));
            effect = Instantiate(successEffects[index]);
            effect.transform.SetParent(parent);
            effect.transform.position = position;
        }
        return effect;
    }

    public GameObject ShowLoseEffect(Transform parent, Vector3 position)
    {
        GameObject effect = null;
        if (loseEffect != null)
        {
            Debug.Log("ShowLoseEffect");
            effect = Instantiate(loseEffect);
            effect.transform.SetParent(parent);
            effect.transform.position = position;
        }
        return effect;
    }

}
