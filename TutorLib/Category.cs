﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TutorLib
{
    [Serializable]
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ApplicationUser[] Users { get; set; }
        public Bundle[] Bundles { get; set; }


    }
}
