﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameModes { reading, math, combo };
public enum GameState { initial, mystery, model, text, math, recognizedCorrect, recognizedUncorrect };



public class StatesScript : MonoBehaviour {

    public Image puzzleRed;
    public Image puzzleGreen;
    public Image puzzleOrange;
    public Image puzzleBlue;

    private AudioClip successClip;
    private AudioClip firstPuzzleClip;
    private AudioSource audioData;


    public static GameModes mode;

    public static GameState State { get; set; }

    public static bool CurrentResult { get; set; }
    public static bool IncrementScore { get; set; }

    public static int Score { get; set; }


    public static int TriesCounter = 0;  //3 tries per task

    private ObjectsScript objectsHolder;
    private string locale;



    // Use this for initialization
    void Start () {
        Score = ServerClientScript.User != null ? (int)ServerClientScript.User.Score : 0;
        objectsHolder = this.GetComponent<ObjectsScript>();
        audioData = GetComponent<AudioSource>();

        locale = ServerClientScript.User != null && ServerClientScript.User.Locale != null && ServerClientScript.User.Locale != string.Empty ?
           ServerClientScript.User.Locale : Application.systemLanguage == SystemLanguage.Russian ?
           "ru-RU" : "en-US";

        successClip = SoundsSource.Instance.GetClip("success", locale);
        
        firstPuzzleClip = SoundsSource.Instance.GetClip("first_puzzle", locale);


        CurrentResult = false;
      
        State = GameState.mystery;
    }

    // Update is called once per frame
    private void Update()
    {
        if (IncrementScore == true)
        {
            Score++;

            if (Score == 5)
            {
                audioData.PlayOneShot(firstPuzzleClip, 0.7F);
            }
            else
            {
                successClip = SoundsSource.Instance.GetClip("success", locale);
                audioData.PlayOneShot(successClip, 0.7F);
            }

            //show puzzle:
            if (Score % 20 == 0)
            {
                puzzleBlue.GetComponent<PuzzleAnimationScript>().ShowAppearAnimation();
            }
            else if (Score % 15 == 0)
            {
                puzzleOrange.GetComponent<PuzzleAnimationScript>().ShowAppearAnimation();
            }
            if (Score % 10 == 0)
            {
                puzzleGreen.GetComponent<PuzzleAnimationScript>().ShowAppearAnimation();
            }
            if (Score%5 == 0)
            {
                puzzleRed.GetComponent<PuzzleAnimationScript>().ShowAppearAnimation();
            }


            //update score on server:
            StartCoroutine(ServerClientScript.SetScore(Score));
            IncrementScore = false;
        }

        //if task not completed after 3 tries
        if (State == GameState.mystery && TriesCounter >= 3)
        {
            TriesCounter = 0;
            objectsHolder.ShowTaskDestroy();
        }


    }


}
