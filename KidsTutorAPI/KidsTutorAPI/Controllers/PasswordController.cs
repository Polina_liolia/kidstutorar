﻿using KidsTutorAPI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace KidsTutorAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PasswordController : Controller, IDisposable
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        //  ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>());

        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = db.Users.FirstOrDefault(u => u.UserName == model.Email);
            if (user == null)
            {
                return View("Error");
            }

            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return View("PasswordSuccess");
            }
            else
            {
                return View("Error");
            }
            
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("Account/Password/ConfirmEmailSuccess")]
        public ActionResult ConfirmEmailSuccess()
        {
            return View("ConfirmEmailSuccess");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}