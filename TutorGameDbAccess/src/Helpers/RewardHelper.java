package Helpers;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import tutorgamedbaccess.Reward;
import tutorgamedbaccess.Reward;

public class RewardHelper {
public EntityManager em = Persistence.createEntityManagerFactory("TutorGameDbAccessPU").createEntityManager();

    public Reward add(Reward reward){
        em.getTransaction().begin();
        Reward rewardFromDB = em.merge(reward);
        em.getTransaction().commit();
        return rewardFromDB;
    }

    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    public Reward get(long id){
        return em.find(Reward.class, id);
    }

    public void update(Reward reward){
        em.getTransaction().begin();
        em.merge(reward);
        em.getTransaction().commit();
    }

    public List<Reward> getAll(){
        TypedQuery<Reward> namedQuery = em.createNamedQuery("Reward.getAll", Reward.class);
        return namedQuery.getResultList();
    }
}
