﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsScript : MonoBehaviour {

    AudioSource audioData;
    public AudioClip SuccessSound;
    public AudioClip LoseSound;
    public AudioClip BoxCrashSound;
    public AudioClip DestroyWordSound;
    
    void Start () {
        //initialise audio source:
        audioData = GetComponent<AudioSource>();
    }

    public void Success()
    {
        audioData.PlayOneShot(SuccessSound, 0.7F);
    }

    public void Lose()
    {
        audioData.PlayOneShot(LoseSound, 0.7F);
    }

    public void BoxCrash()
    {
        audioData.PlayOneShot(BoxCrashSound, 0.7F);
    }

    public void WordDestroing()
    {
        audioData.PlayOneShot(DestroyWordSound, 0.7F);
    }


}
