﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KidsTutor.Models
{
    [Serializable]
    public class User
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        [Required]
        [Range (1, 3, ErrorMessage = "ComplexityLevel has to be from 1 to 3")]
        public int ComplexityLevel { get; set; }
        [Required]
        public long Score { get; set; }
        [Required]
        public int HelpAvailable { get; set; }

        public virtual IList<CategoryUser> CategoryUsers { get; set; }

        public User() {
            CategoryUsers = new List<CategoryUser>();
        }

        public User(string name, DateTime bdate, int complexityLevel = 1, long score = 0L, int help = 5)
        {
            Name = name;
            BirthDate = bdate;
            ComplexityLevel = complexityLevel;
            Score = score;
            HelpAvailable = help;

            CategoryUsers = new List<CategoryUser>();

        }
    }
}
