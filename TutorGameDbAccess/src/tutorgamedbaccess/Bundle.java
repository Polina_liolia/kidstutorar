package tutorgamedbaccess;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id; 
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQuery(name = "Bundle.getAll", query = "SELECT b from Bundle b")
@Table(name="Bundles")
public class Bundle implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    private String name;
    @Column(name = "Path")
    private String bundle_path;
    private String assetName;
  //  private String word;
  //  private int complexityLevel;
    private boolean isAction;
    
    public Bundle() {
    }

    public Bundle(String name, String bundle_path, String assetName, boolean isAction) {
        this.name = name;
        this.bundle_path = bundle_path;
        this.assetName = assetName;
      //  this.word = word;
      //  this.complexityLevel = complexityLevel;
        this.isAction = isAction;
    }
    
//    @ManyToMany(cascade = { 
//        CascadeType.PERSIST, 
//        CascadeType.MERGE
//    })
//    @JoinTable(name = "BundleCategories",
//        joinColumns = @JoinColumn(name = "BundleId"),
//        inverseJoinColumns = @JoinColumn(name = "CategoryId")
//    )
    //private Set<Category> categories = new HashSet<>() ;

//    public void addCategory(Category category) {
//        categories.add(category);
//        category.getBundles().add(this);
//    }
// 
//    public void removeCategory(Category category) {
//        categories.remove(category);
//        category.getBundles().remove(this);
//    }
//
//    public Set<Category> getCategories() {
//        return categories;
//    }
//
//    public void setCategories(Set<Category> categories) {
//        this.categories = categories;
//    }
    
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBundle_path() {
        return bundle_path;
    }

    public void setBundle_path(String bundle_path) {
        this.bundle_path = bundle_path;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.bundle_path);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bundle other = (Bundle) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bundle{" + "id=" + id + ", name=" + name + ", bundle_path=" + bundle_path + ", assetName=" + assetName + '}';
    }

    /**
     * @return the isAction
     */
    public boolean getIsAction() {
        return isAction;
    }

    /**
     * @param isAction the isAction to set
     */
    public void setIsAction(boolean isAction) {
        this.isAction = isAction;
    }
    
}
