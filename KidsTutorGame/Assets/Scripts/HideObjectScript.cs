﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideObjectScript : MonoBehaviour {

	public void HideObject(GameObject _object)
    {
        _object.SetActive(false);
    }
}
