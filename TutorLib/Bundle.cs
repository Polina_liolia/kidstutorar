﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TutorLib { 

    [Serializable]
    public class Bundle
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string AssetName { get; set; }
        public string Word { get; set; }
        public bool IsAction { get; set; }
        public int ComplexityLevel { get; set; }

        public Category[] Categories { get; set; }

       
    }
}
