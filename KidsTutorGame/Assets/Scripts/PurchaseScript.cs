﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PurchaseScript : MonoBehaviour {

    public GameObject Preloader;
    public GameObject ErrorPopup;

    public void OnPurchaseSucceded()
    {
        StartCoroutine(getPremium());
    }

    public void OnPurchaseFailed()
    {
        ErrorPopup.SetActive(true);
    }

    private IEnumerator getPremium()
    {
        Preloader.SetActive(true);
        //get bundles
        yield return StartCoroutine(ServerClientScript.GetPremium());
        Preloader.SetActive(false);
        SceneManager.LoadScene(sceneName: "MenuScene");
        Debug.Log($"NextScene: MenuScene");
    }
}
