﻿using SpeechToTextLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClickActionScript : MonoBehaviour
{
    private GameObject microphoneBtn;
    private bool isRecording = false;
    private bool isRecordMax = false;
    private SpeechToTextConverter speechToTextConverter;
    private string wordRecognized = "";

    private AudioClip loseSound;
    private AudioClip tryAgainSound;
    private AudioSource audioData;

    public GameObject charBoy;
    public GameObject progressBar;
    public GameObject Preloader;

    private CharAnimationsScript charAnimator;

    private Timer aTimer;
    private DateTime dateStart;
    private DateTime dateStop;

    private string locale;



    private void Start()
    {
        //initialise audio source:
        audioData = GetComponent<AudioSource>();
        charAnimator = charBoy.GetComponent<CharAnimationsScript>();
        progressBar.SetActive(false);

        microphoneBtn = GameObject.Find("MicrophoneBtn");
        locale = ServerClientScript.User != null && !ServerClientScript.User.Locale.Equals(string.Empty) ?
            ServerClientScript.User.Locale : Application.systemLanguage == SystemLanguage.Russian ?
            "ru-RU" : "en-US";

        loseSound = SoundsSource.Instance.GetClip("wrong", locale); 
        tryAgainSound = SoundsSource.Instance.GetClip("oops", locale); 

        speechToTextConverter = new SpeechToTextConverter(this, "AIzaSyBNwOWkSJYf96uJLSoFq2lEGLNrOA1KArw", locale);
        speechToTextConverter.CheckMicrophone();
    }

    private void Update()
    {
        if(StatesScript.State != GameState.text)
        {
            microphoneBtn.GetComponent<Button>().interactable = false;
        }
        else if (!isRecording)
        {
            microphoneBtn.GetComponent<Button>().interactable = true;
        }

        if (isRecordMax)
        {
            Debug.Log($"Record max in {(dateStop - dateStart).Milliseconds} ms");
            StopRecording();
            //enable microphone button:
            microphoneBtn.GetComponent<Button>().interactable = true;
            isRecordMax = false;
        }
    }


    public void OnMouseDown()
    {
        if (!isRecording)
        {
            //disable microphone button:
            microphoneBtn.GetComponent<Button>().interactable = false;
            Debug.Log("Start recording");
            isRecording = true;
            progressBar.SetActive(true);
            speechToTextConverter.StartRecording();

            dateStart = DateTime.Now;
            //stop recording in 5 seconds if not stopped before
            SetTimer(5000, (System.Object source, ElapsedEventArgs e) =>
            {
                Debug.Log($"Timeout!");
                aTimer.Stop();
                aTimer.Dispose();
                dateStop = DateTime.Now;
                isRecordMax = true;
            });

        }
    }

    public void OnMouseUp()
    {
       // StopRecording();
    }


    IEnumerator Recognize()
    {
        Preloader.SetActive(true);
        yield return StartCoroutine(speechToTextConverter.ProcessRecord(word => wordRecognized = word));

        Preloader.SetActive(false);

        if (wordRecognized.Equals("error")) //some error on Google API request
        {
            audioData.PlayOneShot(tryAgainSound, 0.7F);
            yield break;

        }

        //compare text from speech with word:
        bool isCorrect = SpeechToWordComparer.Compare(ObjectsScript.CurrentWord, wordRecognized);

        if (isCorrect) //success
        {
            Debug.Log("Correct!");
            StatesScript.State = GameState.model; //model state
            StatesScript.CurrentResult = true;
            StatesScript.IncrementScore = true;
            ObjectsScript.ModelHasToBeLoaded = true;
        }
        else //wron word
        {
            Debug.Log(string.Format("Wrong word: you said {0}, correct is {1}", wordRecognized, ObjectsScript.CurrentWord));
            StatesScript.TriesCounter++; //play lose sound
            if (StatesScript.TriesCounter > 0)
            {
                loseSound = SoundsSource.Instance.GetClip("wrong", locale);
                audioData.PlayOneShot(loseSound, 0.7F);
                charAnimator.Sad(); //char's reaction
            }
            if (StatesScript.TriesCounter >= 3)
            {
                StatesScript.State = GameState.mystery; //mystery state
                charAnimator.Idle(); //char's reaction
            }
        }
    }

    private void SetTimer(long msDuration, ElapsedEventHandler callback)
    {
        // Create a timer with interval.
        aTimer = new Timer(msDuration);
        // Hook up the Elapsed event for the timer. 
        aTimer.Elapsed += callback;//(sender, e) => OnTimedEvent(sender, e);
        aTimer.AutoReset = true;
        aTimer.Enabled = true;
    }

    private void StopRecording()
    {
        if (isRecording)
        {
            Debug.Log("ProcessRecord");
            isRecording = false;
            progressBar.SetActive(false);
            resetProgressBar();
            Preloader.SetActive(false);
            StartCoroutine(Recognize());
        }
    }

    private void resetProgressBar()
    {
        Animation anim = progressBar.GetComponent<Animation>();
        foreach (AnimationState state in anim)
        {
            state.time = 0.0F;
        }
    }
}
