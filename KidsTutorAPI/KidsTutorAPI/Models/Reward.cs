﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace KidsTutorAPI.Models
{
    [Serializable]
    [DataContract]
    public class Reward
    {
        [Key]
        [DataMember]
        public int Id { get; set; }
        [Required]
        [DataMember]
        public string Name { get; set; }
        [Required]
        [DataMember]
        public string Url { get; set; }
        [Required]
        [DataMember]
        public int Count { get; set; }

        public IList<ApplicationUser> Users { get; set; }

        public Reward()
        {
            Users = new List<ApplicationUser>();
        }

        public Reward(string name, string url, int count)
        {
            Users = new List<ApplicationUser>();
            Name = name;
            Url = url;
            Count = count;
        }
    }
}