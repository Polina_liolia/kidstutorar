﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace KidsTutorAPI.Models
{
    [Serializable]
    [DataContract]
    public class Calculation
    {
        public int Id { get; set; }
        [DataMember]
        public int FirstPart { get; set; }
        [DataMember]
        public int SecondPart { get; set; }
        [DataMember]
        public string Sign { get; set; }
        [DataMember]
        public int Result { get; set; }

        public Calculation(int firstPart, int secondPart, string sign)
        {
            FirstPart = firstPart;
            SecondPart = secondPart;
            Sign = sign;
            Result = Sign == "+" ? FirstPart + SecondPart :
                Sign == "-" ? FirstPart - SecondPart :
                Sign == "x" ? FirstPart * SecondPart :
                Sign == ":" ? FirstPart / SecondPart : -1;
        }
    }
}