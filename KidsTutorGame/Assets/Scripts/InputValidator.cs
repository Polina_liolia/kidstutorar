﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Mail;

public class InputValidator : MonoBehaviour {

    public static bool IsEmailValid(string email)
    {
        try
        {
            MailAddress address = new MailAddress(email);
            return true;
        }
        catch
        {
            return false;
        }
    }
}
