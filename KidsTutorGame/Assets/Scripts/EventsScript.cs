﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventsScript : MonoBehaviour {

    private ObjectsScript objectsHolder;

    // Use this for initialization
    void Start () {
        objectsHolder = this.GetComponent<ObjectsScript>();
    }
	
	// Update is called once per frame
	void Update () {

        //touch event:
        foreach (Touch touch in Input.touches)
        {

            RaycastHit hit;
            // Construct a ray from the current touch coordinates
            var ray = Camera.main.ScreenPointToRay(touch.position);
            if (Physics.Raycast(ray, out hit))
            {
                if (touch.phase == TouchPhase.Began)
                    ProcessMouseDownInput(hit);
                Debug.Log("You touched a " + hit.collider.gameObject.name);
            }
        }

        //mouse down and mouse up event
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
        {
            RaycastHit hit;
            // Construct a ray from the current touch coordinates
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (Input.GetMouseButtonDown(0))
                    ProcessMouseDownInput(hit);
                Debug.Log("Mouse down on " + hit.collider.gameObject.name);
            }
        }
    }

    private void ProcessMouseDownInput(RaycastHit hit)
    {
        GameObject targetObject = hit.collider.gameObject;
        if (targetObject.name == objectsHolder.mysteryBoxName)
        {
            objectsHolder.OnMysteryBoxClicked(targetObject);
        }
        else if (targetObject.name == ObjectsScript.CurrentBundleData.Name && StatesScript.State == GameState.model)
        {
            objectsHolder.OnModelClicked(targetObject);
        }
    }
}
