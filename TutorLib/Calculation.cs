﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorLib
{
    [Serializable]
    public class Calculation
    {
        public int Id { get; set; }
        public int FirstPart { get; set; }
        public int SecondPart { get; set; }
        public char Sign { get; set; }
        public int Result { get; set; }

        public Calculation(int firstPart, int secondPart, char sign)
        {
            FirstPart = firstPart;
            SecondPart = secondPart;
            Sign = sign;
            Result = Sign == '+' ? FirstPart + SecondPart :
                Sign == '-' ? FirstPart - SecondPart :
                Sign == 'x' ? FirstPart * SecondPart :
                Sign == ':' ? FirstPart / SecondPart : -1;
        }
    }
}
