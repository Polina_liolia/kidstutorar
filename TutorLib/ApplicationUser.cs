﻿using System;
using System.Collections.Generic;

namespace TutorLib
{
    [Serializable]
    public class ApplicationUser
    {
        public int ComplexityLevel { get; set; }
        public long Score { get; set; }
        public Reward[] Rewards { get; set; }

    }
}