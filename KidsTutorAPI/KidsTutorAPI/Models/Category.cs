﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace KidsTutorAPI.Models
{
    [Serializable]
    [DataContract]
    public class Category
    {
        [Key]
        [DataMember]
        public int Id { get; set; }
        [Required]
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }

        public IList<ApplicationUser> Users { get; set; }
        public IList<Bundle> Bundles { get; set; }


        public Category()
        {
            Users = new List<ApplicationUser>();
            Bundles = new List<Bundle>();
        }

        public Category(string name, string description = "")
        {
            Name = name;
            Description = description;
            Users = new List<ApplicationUser>();
            Bundles = new List<Bundle>();
        }
    }
}
