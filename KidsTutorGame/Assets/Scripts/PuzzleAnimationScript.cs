﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleAnimationScript : MonoBehaviour {

    //00aa44 green
    //d45500 orange
    //0066ff blue

    Animator animator;
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
    }

    public void ShowAppearAnimation()
    {
        animator.SetTrigger("AppearTrigger");
    }
}
