﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KidsTutor.Models
{
    [Serializable]
    public class Category
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public IList<CategoryUser> CategoryUsers { get; set; }
        public IList<BundleCategory> BundleCategories { get; set; }


        public Category()
        {
            CategoryUsers = new List<CategoryUser>();
            BundleCategories = new List<BundleCategory>();
        }

        public Category(string name, string description = "")
        {
            Name = name;
            Description = description;
            CategoryUsers = new List<CategoryUser>();
            BundleCategories = new List<BundleCategory>();
        }
    }
}
