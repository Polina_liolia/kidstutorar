﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeadersScript : MonoBehaviour {

    public Transform parent;
    public GameObject rowPrefab;
    public GameObject[] txtTmp;
    public GameObject Preloader;

    private ApplicationUser[] leaders;


	// Use this for initialization
	void Start () {
        StartCoroutine(ShowLeaders());
	}
	
	private IEnumerator ShowLeaders()
    {
        //getting leaders list from server
        yield return StartCoroutine(ServerClientScript.GetLeaders());
        leaders = ServerClientScript.Leaders;
        if(leaders != null && leaders.Length > 0)
        {
            for(int i = 0; i < txtTmp.Length; i++)
            {
                txtTmp[i].SetActive(false);
            }
           
            for (int i = 0; i < leaders.Length; i++)
            {
                ApplicationUser leader = leaders[i];
                bool isCurrentPlayer = ServerClientScript.User != null && leader.Email.Equals(ServerClientScript.User.Email);
                GameObject row = Instantiate(rowPrefab);
                row.name = $"row_{i + 1}";
                row.transform.SetParent(parent);
                row.transform.localPosition = new Vector3(0, -160 - 50 * i + 400, 1);

                //set value to row children:
                for (int j = 0; j < row.transform.childCount; ++j)
                {
                    Transform currentItem = row.transform.GetChild(j);

                    //Search by name
                    if (currentItem.name.Equals("txt_number"))
                    {
                        currentItem.GetComponent<Text>().text = $"{i + 1}";
                        if (isCurrentPlayer)
                            currentItem.GetComponent<Text>().color = Color.red;
                    }
                    else if (currentItem.name.Equals("txt_name"))
                    {
                        currentItem.GetComponent<Text>().text = leader.Name;
                        if (isCurrentPlayer)
                            currentItem.GetComponent<Text>().color = Color.red;
                    }
                    else if (currentItem.name.Equals("txt_score"))
                    {
                        currentItem.GetComponent<Text>().text = $"{leader.Score}";
                        if (isCurrentPlayer)
                            currentItem.GetComponent<Text>().color = Color.red;
                    }

                }
            }
        }
        else
        {
            Debug.Log("No leaders to show");
        }
        Preloader.SetActive(false);
    }
}
