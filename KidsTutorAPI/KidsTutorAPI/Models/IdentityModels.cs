﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace KidsTutorAPI.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    [Serializable]
    [DataContract]
    public class ApplicationUser : IdentityUser
    {
        private IList<Category> categories = new List<Category>();
        private IList<Reward> rewards = new List<Reward>();

        [DataMember]
        public override string Id { get => base.Id; set => base.Id = value; }

        [DataMember]
        public override string UserName { get => base.UserName; set => base.UserName = value; }

        [DataMember]
        public override string Email { get => base.Email; set => base.Email = value; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int ComplexityLevel { get; set; }

        [DataMember]
        public long Score { get; set; }

        [DataMember]
        public string Locale { get; set; } 

        [DataMember]
        public bool IsPremium { get; set; }

        [DataMember]
        public IList<Reward> Rewards { get => rewards; set => rewards = value; }

        
        public IList<Category> Categories { get => categories; set => categories = value; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here

            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Bundle> Bundles { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Reward> Rewards { get; set; }
        public DbSet<Word> Words { get; set; }


        public ApplicationDbContext() 
            : base("TutorGameDbConnAws", throwIfV1Schema: false)
           // : base("TutorGameDbConn", throwIfV1Schema: false)
        {
        }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Bundle>()
                .HasMany(b => b.Categories)
                .WithMany(c => c.Bundles);

            modelBuilder.Entity<Category>()
                .HasMany(b => b.Users)
                .WithMany(u => u.Categories);

            modelBuilder.Entity<Category>()
                 .HasMany(c => c.Bundles)
                 .WithMany(b => b.Categories);

            modelBuilder.Entity<Bundle>()
                .HasMany(b => b.Words)
                .WithRequired(w => w.Bundle);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(u => u.Categories)
                .WithMany(c => c.Users);

            modelBuilder.Entity<ApplicationUser>()
                 .HasMany(u => u.Rewards)
                 .WithMany(r => r.Users);

        }   
    }
}