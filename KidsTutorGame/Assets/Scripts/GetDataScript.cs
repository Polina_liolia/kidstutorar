﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class GetDataScript : MonoBehaviour
{
    public Bundle[] bundles;
    public Calculation[] math;

    private int bundlesCounter = 0;
    private int mathCounter = 0;

    private void Start()
    {
        StartCoroutine(GetBundlesData());
        StartCoroutine(GetMathData());
    }

    private IEnumerator GetBundlesData()
    {
        int count = 60; 
        //get bundles
        yield return StartCoroutine(ServerClientScript.GetBundles(count, (b) => bundles = b));
    }

    private IEnumerator GetMathData()
    {
        int count = 5;
        //get math
        yield return StartCoroutine(ServerClientScript.GetMath(count, (m) => math = m));
    }

    public Bundle GetMysteryBox()
    {
        if (bundles == null || bundles.Length == 0)
            GetBundlesData();
        return bundles[0];
    }

    public Bundle GetNextBundle()
    {
        int index = 0;
        if(bundles == null)
        {
            Debug.Log("Bundles array is null!");
            return null;
        }
        else if (bundlesCounter == bundles.Length-1)//the last element
        {
            index = bundlesCounter;
            bundlesCounter = 0;
            StartCoroutine(GetBundlesData()); //preparing for the next item request
        }
        else if (bundlesCounter < bundles.Length)
        {
            index = bundlesCounter++;
        }
        //new System.Random().Next(1, this.bundles.Count);
        Debug.Log($"GetNext: {bundles[index].Name}");
        return bundles[index];
    }

    public Calculation GetNextMath()
    {
        int index = 0;
        if (math == null)
        {
            Debug.Log("Math array is null!");
            return null;
        }
        else if (mathCounter == math.Length - 1)//the last element
        {
            index = mathCounter;
            mathCounter = 0;
            StartCoroutine(GetMathData()); //preparing for the next item request
        }
        else if (mathCounter < math.Length)
        {
            index = mathCounter++;
        }
        //new System.Random().Next(1, this.bundles.Count);
        Debug.Log($"GetNextMath: {math[index].FirstPart} {math[index].Sign} {math[index].SecondPart}");
        return math[index];
    }
}
