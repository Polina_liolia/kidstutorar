﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SpeechToWordComparer {

    public static string[] SpecialChars = { ".", ",", "!", "\"", "?", "'", "/", 
    "*", "@", "#", "%", "(", ")", "$", ";", ":", "&", "+", "=", "-" };

    public static bool Compare(string basicWord, string recognizedString)
    {
        bool isEqual = false;

        //set both words to lower to avoid mismatch:
        basicWord = basicWord.ToLower();
        recognizedString = recognizedString.ToLower();

        //remove all special characters:
        foreach(string specChar in SpecialChars)
        {
            recognizedString.Replace(specChar, "");
        }


        //get all words in recognozed string:
        string[] allRecognizedWords = recognizedString.Split(' ');
        isEqual = arrayContainsWord(allRecognizedWords, basicWord);

        //try with corrections
        if (!isEqual)
        {
            isEqual = arrayContainsCorrectedWord(allRecognizedWords, basicWord);
        }

        return isEqual;
    }

    //checks if array contains word from args
    private static bool arrayContainsWord(string[] arr, string word)
    {
        bool contains = false;
        foreach (string current in arr)
        {
            if (word.Equals(current))
            {
                contains = true;
                break;
            }
        }
        return contains;
    }

    //checks if array contains word with match percent 75% or more
    private static bool arrayContainsCorrectedWord(string[] arr, string word)
    {
        bool contains = false;
        int basicWordLength = word.Length;

        //at least 75% of characters have to match:
        int matchingCharsTarget = Mathf.RoundToInt(basicWordLength * 0.75f);

        foreach (string current in arr)
        {
            int matchCounter = 0;
            for (int i = 0; i < basicWordLength; i++)
            {
                if (current.Length > i && word[i].Equals(current[i]))
                {
                    matchCounter++;
                }
            }

            if (matchCounter >= matchingCharsTarget)// current word matches with corrections
            {
                contains = true;
                break;
            }
        }
        return contains;
    }
}
