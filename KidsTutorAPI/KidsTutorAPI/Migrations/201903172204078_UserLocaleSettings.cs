namespace KidsTutorAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserLocaleSettings : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Locale", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Locale");
        }
    }
}
