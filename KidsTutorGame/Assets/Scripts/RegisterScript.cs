﻿using UnityEngine;
using UnityEngine.UI;
using ServerClientLib;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class RegisterScript : MonoBehaviour {

    public GameObject Preloader;

    public Text textField;
    public InputField NameInput;
    public InputField EmailInput;
    public InputField PasswordInput;
    public InputField PasswordConfirmInput;

    private string email;
    private string userName;
    private string password ;
    private string passwordConfirm;
    private string message = "";

    private string locale;

    private void Start()
    {
        locale = ServerClientScript.User != null && ServerClientScript.User.Locale != null && ServerClientScript.User.Locale != string.Empty ?
           ServerClientScript.User.Locale : Application.systemLanguage == SystemLanguage.Russian ?
           "ru-RU" : "en-US";
        Preloader.SetActive(false);
        NameInput.onEndEdit.AddListener(delegate { OnNameInputEditEnd(NameInput); });
        EmailInput.onEndEdit.AddListener(delegate { OnEmailInputEditEnd(EmailInput); });
        PasswordInput.onEndEdit.AddListener(delegate { OnPasswordInputEditEnd(PasswordInput); });
        PasswordConfirmInput.onEndEdit.AddListener(delegate { OnPasswordConfirmInputEditEnd(PasswordConfirmInput); });
    }

    void OnGUI()
    {
        textField.text = message;

    }

    public void OnNameInputEditEnd(InputField input)
    {
        userName = input.text;
    }

    public void OnEmailInputEditEnd(InputField input)
    {
        email = input.text;
    }

    public void OnPasswordInputEditEnd(InputField input)
    {
        password = input.text;
    }

    public void OnPasswordConfirmInputEditEnd(InputField input)
    {
        passwordConfirm = input.text;
    }

    public void OnRegister()
    {
        if (!InputValidator.IsEmailValid(email))
        {
            string msg = locale.Equals("ru-RU") ? "Введите правильный email" : "Wrong email";
            message = $"{message}{Environment.NewLine}{msg}";
            return;
        }
        if (password.Length < 6)
        {
            string msg = locale.Equals("ru-RU") ? "Пароль полжен включать 6 и более символов" :
                        "Password must contain 6 or more characters";
            message = $"{message}{Environment.NewLine}{msg}";
            return;
        }
            if (password.Equals(passwordConfirm))
        {
            StartCoroutine(Register(email, password));
        }
        else
        {
            string msg = locale.Equals("ru-RU") ? "Пароль не подтвержден" :
                        "Password not confirmed";
            message = $"{message}{Environment.NewLine}{msg}.";
        }

    }

    public IEnumerator Register(string email, string password)
    {
        Preloader.SetActive(true);
        long responseCode = -1;
        string info = string.Empty;
        yield return StartCoroutine(ServerClientScript.Register(email, password, (code, details) => { responseCode = code; info = details; }));
        Preloader.SetActive(false);

        Debug.Log($"Register result: {responseCode}");
        if (responseCode == 200)
            StartCoroutine(LogIn(userName, email, password));
        else
        {
            string msg = locale.Equals("ru-RU") ? "Ошибка регистрации:" :
                       "Sign up error";
            message = $"{message}{Environment.NewLine}{msg}.";
            message = $"{message}{Environment.NewLine}{msg}: {info}";
        }
    }

    public IEnumerator LogIn(string name, string email, string password)
    {
        Preloader.SetActive(true);
        Debug.Log("Log In action...");
        yield return ServerClientScript.GetToken(email, password);
        if (ServerClientScript.IsSignedIn && ServerClientScript.UserToken != null && !ServerClientScript.UserToken.Equals(string.Empty)) //succeeded
        {
            yield return StartCoroutine(ServerClientScript.GetUser());
            yield return StartCoroutine(ServerClientScript.SetName(name));

            Preloader.SetActive(false);
            SceneManager.LoadScene(sceneName: "MenuScene");
        }
        else //failed
        {
            string msg = locale.Equals("ru-RU") ? "Ошибка входа в учетную запись" :
                       "Sign in error";
            message = $"{message}{Environment.NewLine}{msg}";
            Debug.Log("Can't get token");
        }
        Preloader.SetActive(false);
    }
}
