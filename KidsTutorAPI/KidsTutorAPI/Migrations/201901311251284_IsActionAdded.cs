namespace KidsTutorAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsActionAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bundles", "IsAction", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bundles", "IsAction");
        }
    }
}
