﻿using ServerClientLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using UnityEngine;
using UnityEngine.Networking;



public class ServerClientScript : MonoBehaviour {

    public static string Host = "https://www.kidstutorar.es"; //"http://127.0.0.1:8080";

    public static bool IsPremiumAvailable = false;

    public static bool IsSignedIn = false;
    public static string UserToken;
    public static ApplicationUser User { get; set; }

    public static ApplicationUser[] Leaders { get; set; }

    public delegate void ResponseDelegate(long code, string details);
    public delegate void GetBundleDelegate(Bundle[] bundles);
    public delegate void GetMathDelegate(Calculation[] calc);


    public static IEnumerator Register(string email, string password, ResponseDelegate getResponse )
    {
        string url = $"{Host}/api/Account/Register";

        WWWForm form = new WWWForm();
        form.AddField("Email", email);
        form.AddField("Password", password);
        form.AddField("ConfirmPassword", password);

        UnityWebRequest www = UnityWebRequest.Post(url, form);
        www.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        yield return www.SendWebRequest();

        long code = -1;
        string details = string.Empty;

        if (www.isNetworkError)
        {
            string errorInfo = $"{www.error} error code: {www.responseCode}";
            Debug.Log(errorInfo);
            details = errorInfo;
        }
        else
        {
            code = www.responseCode;
            details = www.downloadHandler.text;
            //saving data to file:
            AuthData authData = GameDataController.AuthData;
            authData.Login = email;
            authData.Password = password;
        }
        getResponse(code, details);
    }

    public static IEnumerator GetToken(string email, string password)
    {
        string url = $"{Host}/Token";

        WWWForm form = new WWWForm();
        form.AddField("grant_type", "password");
        form.AddField("username", email);
        form.AddField("Password", password);

        UnityWebRequest www = UnityWebRequest.Post(url, form);
        www.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            string json = www.downloadHandler.text;// "{\"bundles\":" + www.downloadHandler.text + "}";
            Debug.Log($"Token data: {json}");
            LoginData loginData = JsonUtility.FromJson<LoginData>(json);
            UserToken = loginData.access_token;
            IsSignedIn = true;
            //saving data to file:
            AuthData authData = GameDataController.AuthData;
            authData.Login = email;
            authData.Password = password;
            authData.Token = UserToken;
            GameDataController.AuthData = authData;
        }
    }

    public static IEnumerator GetUser()
    {
        string url = $"{Host}/api/user";
        UnityWebRequest www = UnityWebRequest.Get(url);
        //token = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(token));
        www.SetRequestHeader("Authorization", $"Bearer {ServerClientScript.UserToken}");
        yield return www.SendWebRequest();
        Debug.Log("Get user request completed");
        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            string json = www.downloadHandler.text;// "{\"bundles\":" + www.downloadHandler.text + "}";
            Debug.Log(json);
            ApplicationUserJsonRootSingle jsonRoot = JsonUtility.FromJson<ApplicationUserJsonRootSingle>(json);
            User = jsonRoot.result;
            GameDataController.User = User;
        }
    }

    public static IEnumerator GetLeaders()
    {
        string url = $"{Host}/api/users/leaders";
        UnityWebRequest www = UnityWebRequest.Get(url);
        //token = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(token));
        www.SetRequestHeader("Authorization", $"Bearer {ServerClientScript.UserToken}");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            string json = www.downloadHandler.text;// "{\"bundles\":" + www.downloadHandler.text + "}";
            Debug.Log(json);
            ApplicationUserJsonRoot jsonRoot = JsonUtility.FromJson<ApplicationUserJsonRoot>(json);
            Leaders = jsonRoot.results;
        }
    }

    public static IEnumerator SetName(string newName)
    {
        string url = $"{Host}/api/users/set_name";
        byte[] myData = System.Text.Encoding.UTF8.GetBytes($"{{\"newName\": \"{newName}\" }}");
        UnityWebRequest www = UnityWebRequest.Put(url, myData);
        www.SetRequestHeader("Content-Type", "application/json");
        www.SetRequestHeader("Authorization", $"Bearer {ServerClientScript.UserToken}");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            User.Name = newName;
            GameDataController.User = User;
            Debug.Log("Name updated");
        }
    }

    public static IEnumerator SetEmail(string newEmail)
    {
        string url = $"{Host}/api/users/set_email";
        byte[] myData = System.Text.Encoding.UTF8.GetBytes($"{{\"email\": \"{newEmail}\" }}");
        UnityWebRequest www = UnityWebRequest.Put(url, myData);
        www.SetRequestHeader("Authorization", $"Bearer {ServerClientScript.UserToken}");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            User.Email = newEmail;
            User.UserName = newEmail;
            GameDataController.User = User;
            Debug.Log("Email updated");
        }
    }

    public static IEnumerator SetPassword(string oldPassword, string newPassword)
    {
        string url = $"{Host}/api/users/change_password";
        byte[] myData = System.Text.Encoding.UTF8.GetBytes($"{{\"oldPassword\": \"{oldPassword}\", \"newPassword\": \"{newPassword}\" }}");
        UnityWebRequest www = UnityWebRequest.Put(url, myData);
        www.SetRequestHeader("Authorization", $"Bearer {ServerClientScript.UserToken}");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            Debug.Log("Password updated");
        }
    }

    public static IEnumerator ResetPassword(string email)
    {
        string url = $"{Host}/api/Account/ResetPassword";

        WWWForm form = new WWWForm();
        form.AddField("Email", email);

        UnityWebRequest www = UnityWebRequest.Post(url, form);
        www.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            string json = www.downloadHandler.text;
            Debug.Log("Complexity updated");
        }
    }

    public static IEnumerator SetComplexity(int complexity)
    {
        string url = $"{Host}/api/users/set_complexity/{complexity}";
        UnityWebRequest www = UnityWebRequest.Put(url, "changing complexity");
        www.SetRequestHeader("Authorization", $"Bearer {ServerClientScript.UserToken}");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            User.ComplexityLevel = complexity;
            GameDataController.User = User;
            Debug.Log("Complexity updated");
        }
    }

    public static IEnumerator SetScore(int score)
    {
        if (User == null || User.Score >= score)
        {
            Debug.Log("User is not set or Score is less then current");
            yield break;
        }

        if(IsSignedIn)
        {
            string url = $"{Host}/api/users/set_score/{score}";
            UnityWebRequest www = UnityWebRequest.Put(url, "changing score");
            www.SetRequestHeader("Authorization", $"Bearer {ServerClientScript.UserToken}");
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error + " error code: " + www.responseCode);
            }
            else
            {
                User.Score = score;
                GameDataController.User = User;
                Debug.Log("Score updated");
            }
        }
        else
        {
            User.Score = score;
            GameDataController.User = User;
        }
    }

    public static IEnumerator SetLocale(string locale)
    {
        if (IsSignedIn)
        {
            string url = $"{Host}/api/users/set_locale/{locale}";
            UnityWebRequest www = UnityWebRequest.Put(url, "changing locale");
            www.SetRequestHeader("Authorization", $"Bearer {ServerClientScript.UserToken}");
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error + " error code: " + www.responseCode);
            }
            else
            {
                User.Locale = locale;
                GameDataController.User = User;
                Debug.Log("Locale updated");
            }
        }
        else
        {
            User.Locale = locale;
            GameDataController.User = User;
        }
    }

    public static IEnumerator UpdateRewards()
    {
        string url = $"{Host}/api/users/add_reward";
        UnityWebRequest www = UnityWebRequest.Put(url, "updating rewards");
        www.SetRequestHeader("Authorization", $"Bearer {ServerClientScript.UserToken}");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            Debug.Log("Rewards updated");
        }
    }

    //for non-authorized users
    public static IEnumerator GetCurrentRewards(long score)
    {
        string url = $"{Host}/api/rewards/current/{score}";
        UnityWebRequest www = UnityWebRequest.Get(url);
       // www.SetRequestHeader("Authorization", $"Bearer {ServerClientScript.UserToken}");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            string json = www.downloadHandler.text;// 
            Debug.Log(json);
            RewardJsonRoot jsonRoot = JsonUtility.FromJson<RewardJsonRoot>(json);
            User.Rewards = jsonRoot.results;
            GameDataController.User = User;
            Debug.Log("Rewards recived");
        }
    }

    public static IEnumerator GetBundles(int count, GetBundleDelegate getBundles)
    {
        int complexity = User != null ? User.ComplexityLevel : 1;
        string locale = User != null && !User.Locale.Equals(string.Empty) ? User.Locale : "en-US";
        string isFree = User.IsPremium ? "false" : "true";
        string url = $"{Host}/api/bundles/{locale}/{complexity}/{count}/{isFree}";
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            string json = www.downloadHandler.text;// "{\"bundles\":" + www.downloadHandler.text + "}";
            Debug.Log(json);
            BundleJsonRoot jsonRoot = JsonUtility.FromJson<BundleJsonRoot>(json);
            getBundles(jsonRoot.results);
            Debug.Log("Bundles recived");
        }
    }

    public static IEnumerator GetMath(int count, GetMathDelegate getMath)
    {
        int complexity = User != null ? User.ComplexityLevel : 1;
        string url = $"{Host}/api/calculations/{complexity}/{count}";
        UnityWebRequest www = UnityWebRequest.Get(url);
        www.SetRequestHeader("Authorization", $"Bearer {ServerClientScript.UserToken}");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            string json = www.downloadHandler.text;
            Debug.Log(json);
            MathJsonRoot jsonRoot = JsonUtility.FromJson<MathJsonRoot>(json);
            getMath(jsonRoot.results);
            Debug.Log("Calculation recived");
        }
    }

    public static IEnumerator CheckIsPremiumAvailable()
    {
        string url = $"{Host}/api/values/GetPremiumActive";
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            string response = www.downloadHandler.text;
            Debug.Log(response);
            IsPremiumAvailable = response.Equals("true");
        }
    }

    public static IEnumerator GetPremium()
    {
        string url = $"{Host}/api/users/set_premium";
        UnityWebRequest www = UnityWebRequest.Put(url, "getting premium");
        www.SetRequestHeader("Authorization", $"Bearer {ServerClientScript.UserToken}");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            User.IsPremium = true;
            GameDataController.User = User;
            Debug.Log("Premium got");
        }
    }
}
