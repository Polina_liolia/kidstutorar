﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FtpFilesConnection
{
    class Program
    {
        static void Main(string[] args)
        {
            ListFilesFromFTP("ftp.drivehq.com", "polinaliolia", "drivehqgjkbyf12", "", "assets_ios");
        }

        public static void ListFilesFromFTP(string Hostname, string Username, string Password, string localFilesPath, string remoteFTPPath)
        {
            remoteFTPPath = "ftp://" + Hostname + "/" + remoteFTPPath;
            var request = (FtpWebRequest)WebRequest.Create(remoteFTPPath);

            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(Username, Password);
            request.Proxy = null;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            List<string> directories = new List<string>();

            string line = reader.ReadLine();

            while (!string.IsNullOrEmpty(line))
            {
                directories.Add(line);
                line = reader.ReadLine();
            }
            reader.Close();

            using (WebClient ftpClient = new WebClient())
            {
                ftpClient.Credentials = new System.Net.NetworkCredential(Username, Password);

                using (StreamWriter fileStream = File.CreateText("insert.sql"))
                {
                    for (int i = 0; i <= directories.Count - 1; i++)
                    {
                        if (!directories[i].Contains("."))
                        {
                            string fileName = directories[i].ToString();
                            fileStream.WriteLine($"exec[dbo].[insert_bundle] '{fileName}', '', N'', 1, '', 1;");
                            //fileStream.WriteLine($"INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('{fileName}', '{fileName}', '', 0, 1);");
                        }
                        

                    }
                }
            }
            response.Close();
        }
    }
}
