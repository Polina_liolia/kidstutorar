﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace KidsTutorAPI.Models
{
    [DataContract]
    [Serializable]
    public class Word
    {
        [Key]
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Locale { get; set; }

        [DataMember]
        public string Text { get; set; }

        [Required]
        [DataMember]
        [Range(1, 3, ErrorMessage = "ComplexityLevel has to be from 1 to 3")]
        public int ComplexityLevel { get; set; }

        public virtual Bundle Bundle { get; set; }

        public Word() { }

        public Word(string text, string locale, int complexityLevel)
        {
            Text = text;
            Locale = locale;
            ComplexityLevel = complexityLevel;
        }


    }
}