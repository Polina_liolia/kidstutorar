﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KidsTutor.Models
{
    [Serializable]
    public class Bundle
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Path { get; set; }
        [Required]
        public string AssetName { get; set; }
        [Required]
        public string Word { get; set; }
        [Required]
        [Range(1, 3, ErrorMessage = "ComplexityLevel has to be from 1 to 3")]
        public int ComplexityLevel { get; set; }

        public virtual IList<BundleCategory> BundleCategories { get; set; }

        public Bundle()
        {
            BundleCategories = new List<BundleCategory>();
        }
        public Bundle(string name, string path, string assetName, string word, int complexityLevel)
        {
            BundleCategories = new List<BundleCategory>();
            Name = name;
            Path = path;
            AssetName = assetName;
            Word = word;
            ComplexityLevel = complexityLevel;
        }
    }
}
