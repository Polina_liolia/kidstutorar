﻿using System;
using System.Collections;
using ServerClientLib;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChangePasswordScript : MonoBehaviour
{
    public GameObject Preloader;

    public InputField EmailInput;
    public InputField OldPasswordInput;
    public InputField NewPasswordInput;
    public InputField NewPasswordConfirmInput;
    public Text textField;

    private string email = "polina.liolia@gmail.com";
    private string oldPassword = "123456";
    private string password = "123456";
    private string passwordConfirm = "123456";
    private string message = "";

    private string locale;

    private void Start()
    {
        locale = ServerClientScript.User != null && ServerClientScript.User.Locale != null && ServerClientScript.User.Locale != string.Empty ?
           ServerClientScript.User.Locale : Application.systemLanguage == SystemLanguage.Russian ?
           "ru-RU" : "en-US";

        Preloader.SetActive(false);
        if (ServerClientScript.User != null)
        {
            email = ServerClientScript.User.Email;
            EmailInput.text = email;
        }
        else
        {
            string localeMsg = locale.Equals("ru-RU") ? "Вы не вошли в учетную запись" : "You have to sign in first";
            message = $"{message}{Environment.NewLine}{localeMsg}";
            EmailInput.interactable = false;
            OldPasswordInput.interactable = false;
            NewPasswordInput.interactable = false;
            NewPasswordConfirmInput.interactable = false;
        }

        EmailInput.onEndEdit.AddListener(delegate { OnEmailInputEditEnd(EmailInput); });
        OldPasswordInput.onEndEdit.AddListener(delegate { OnOldPasswordInputEditEnd(OldPasswordInput); });
        NewPasswordInput.onEndEdit.AddListener(delegate { OnPasswordInputEditEnd(NewPasswordInput); });
        NewPasswordConfirmInput.onEndEdit.AddListener(delegate { OnPasswordConfirmInputEditEnd(NewPasswordConfirmInput); });
    }

    void OnGUI()
    {
        textField.text = message;
    }

    public void OnEmailInputEditEnd(InputField input)
    {
        email = input.text;
    }

    public void OnOldPasswordInputEditEnd(InputField input)
    {
        oldPassword = input.text;
    }

    public void OnPasswordInputEditEnd(InputField input)
    {
        password = input.text;
    }

    public void OnPasswordConfirmInputEditEnd(InputField input)
    {
        passwordConfirm = input.text;
    }


    public void OnPasswordChange()
    {

        if (ServerClientScript.IsSignedIn)
        {
            if (password.Equals(string.Empty) && !email.Equals(ServerClientScript.User.Email)) //only email was changed
            {
                if (InputValidator.IsEmailValid(email))
                {
                    Preloader.SetActive(true);
                    StartCoroutine(ChangeAccauntData(email));

                    //saving in filesystem:
                    AuthData authData = GameDataController.AuthData;
                    authData.Token = null; //need to get new token
                    authData.Login = email;
                    GameDataController.AuthData = authData;
                }
                else
                {
                    string msg = locale.Equals("ru-RU") ? "Вы ввели некорректный email" : "Wrong email";
                    message = $"{message}{Environment.NewLine}{msg}";
                }
            }
            else if (!password.Equals(string.Empty) && (email.Equals(ServerClientScript.User.Email) || email.Equals(string.Empty))) //only password was changed
            {
                if (password.Length < 6)
                {
                    string msg = locale.Equals("ru-RU") ? "Пароль полжен включать 6 и более символов" : 
                        "Password must contain 6 or more characters";
                    message = $"{message}{Environment.NewLine}{msg}";
                }
                else if (password.Equals(passwordConfirm))
                {
                    Preloader.SetActive(true);
                    message = "";
                    StartCoroutine(ServerClientScript.SetPassword(oldPassword, password));

                    //saving in filesystem:
                    AuthData authData = GameDataController.AuthData;
                    authData.Token = null; //need to get new token
                    authData.Login = email;
                    GameDataController.AuthData = authData;

                    //changing scene:
                    SceneManager.LoadScene(sceneName: "SettingsScene");
                }
                else
                {
                    string msg = locale.Equals("ru-RU") ? "Новый пароль не подтвержден" :
                       "New password not confirmed";
                    message = $"{message}{Environment.NewLine}{msg}.";
                    Debug.Log("New password doesn't match");
                }
            }
            else //both email and password changed
            {
                if (password.Equals(passwordConfirm) && password.Length >= 6)
                {
                    Preloader.SetActive(true);
                    message = "";
                    StartCoroutine(ChangeAccauntData(email, password));

                    //saving in filesystem:
                    AuthData authData = GameDataController.AuthData;
                    authData.Token = null; //need to get new token
                    authData.Login = email;
                    authData.Password = password;
                    GameDataController.AuthData = authData;

                    //changing scene:
                    SceneManager.LoadScene(sceneName: "SettingsScene");
                }
                else
                {
                    Preloader.SetActive(true);
                    StartCoroutine(ChangeAccauntData(email));
                    string msg = locale.Equals("ru-RU") ? "Email изменен. Новый пароль не подтвержден." :
                       "Email changed. New password not confirmed";
                    message = $"{message}{Environment.NewLine}{msg}.";
                    Debug.Log("Email changed. New password doesn't match");
                }
            }
        }
    }

    public IEnumerator ChangeAccauntData(string email, string password)
    {
        yield return StartCoroutine(ChangeAccauntData(email));
        yield return StartCoroutine(ServerClientScript.SetPassword(oldPassword, password));
        StartCoroutine(LogIn()); //need to log in again
    }

    public IEnumerator ChangeAccauntData(string email)
    {
        yield return StartCoroutine(ServerClientScript.SetEmail(email));
        yield return StartCoroutine(LogIn()); //need to log in again
    }

    private IEnumerator LogIn()
    {
        AuthData authData = GameDataController.AuthData;
        yield return ServerClientScript.GetToken(authData.Login, authData.Password);
        if (ServerClientScript.IsSignedIn && ServerClientScript.UserToken != null && !ServerClientScript.UserToken.Equals(string.Empty)) //succeeded
        {
            yield return StartCoroutine(ServerClientScript.GetUser());
            if (ServerClientScript.User != null) //succeeded
            {
                GameDataController.User = ServerClientScript.User;
            }
            else //failed
            {
                ServerClientScript.IsSignedIn = false;
                ServerClientScript.User = GameDataController.User;
                string msg = locale.Equals("ru-RU") ? "Ошибка входа с новыми учетными данными" :
                       "Sign in error";
                message = $"{message}{Environment.NewLine}{msg}";
            }
        }
        else //failed
        {
            string msg = locale.Equals("ru-RU") ? "Ошибка входа с новыми учетными данными" :
                       "Sign in error";
            message = $"{message}{Environment.NewLine}{msg}";
            Debug.Log("Can't get token");
            ServerClientScript.IsSignedIn = false;
            ServerClientScript.User = GameDataController.User;
        }
        Preloader.SetActive(false);
    }
}
