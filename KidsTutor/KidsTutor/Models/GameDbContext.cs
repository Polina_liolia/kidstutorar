﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KidsTutor.Models
{
    public class GameDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Bundle> Bundles { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<BundleCategory> BundleCategories { get; set; }
        public DbSet<CategoryUser> CategoryUsers { get; set; }

        public GameDbContext() : base()
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Category>()
                .HasKey(c => c.Id);
            modelBuilder.Entity<User>()
                .HasKey(c => c.Id);
            modelBuilder.Entity<Bundle>()
                .HasKey(c => c.Id);
            modelBuilder.Entity<BundleCategory>()
                .HasKey(c => c.Id);
            modelBuilder.Entity<CategoryUser>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<Bundle>()
                .HasMany(b => b.BundleCategories)
                .WithOne(bc => bc.Bundle);

            modelBuilder.Entity<Category>()
                .HasMany(b => b.BundleCategories)
                .WithOne(bc => bc.Category);

            modelBuilder.Entity<BundleCategory>()
               .HasOne(bc => bc.Bundle)
               .WithMany(b => b.BundleCategories)
               .HasForeignKey(bc => bc.BundleId);

            modelBuilder.Entity<BundleCategory>()
               .HasOne(bc => bc.Category)
               .WithMany(b => b.BundleCategories)
               .HasForeignKey(bc => bc.CategoryId);

            modelBuilder.Entity<Category>()
                .HasMany(b => b.CategoryUsers)
                .WithOne(bc => bc.Category);

            modelBuilder.Entity<User>()
                .HasMany(b => b.CategoryUsers)
                .WithOne(bc => bc.User);

            modelBuilder.Entity<CategoryUser>()
               .HasOne(bc => bc.Category)
               .WithMany(b => b.CategoryUsers)
               .HasForeignKey(bc => bc.CategoryId);

            modelBuilder.Entity<CategoryUser>()
               .HasOne(bc => bc.User)
               .WithMany(b => b.CategoryUsers)
               .HasForeignKey(bc => bc.UserId);

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Database=TutorGameDb;AttachDbFilename=D:\KidsTutorAR\KidsTutor\KidsTutorDb.mdf;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                optionsBuilder.UseSqlServer(@"Data Source=tutorgamedb.chixwqmiiwpe.us-east-2.rds.amazonaws.com; Initial Catalog=GameDb; User ID =polina; Password=tutorgamedb123;");
            }
        }
    }
}
