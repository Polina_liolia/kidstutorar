﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {

    public Button btnPlay, btnRewards, btnLeaders;
	// Use this for initialization
	void Start () {
        btnPlay.onClick.AddListener(() => ShowScene("MainScene"));

    }

    void ShowScene(string nextSceneName)
    {
        Debug.Log("NextScene btn clicked");
        if (!nextSceneName.Equals(""))
        {
            SceneManager.LoadScene(sceneName: nextSceneName);
            Debug.Log($"NextScene: {nextSceneName}");
        }
        else
        {
            Debug.Log("No scene name provided");
        }
    }
}
