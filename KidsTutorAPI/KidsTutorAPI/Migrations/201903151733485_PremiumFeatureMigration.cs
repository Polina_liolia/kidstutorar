namespace KidsTutorAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PremiumFeatureMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bundles", "IsFree", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "IsPremium", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsPremium");
            DropColumn("dbo.Bundles", "IsFree");
        }
    }
}
