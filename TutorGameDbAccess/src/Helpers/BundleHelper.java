package Helpers;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import tutorgamedbaccess.Bundle;

public class BundleHelper {
public EntityManager em = Persistence.createEntityManagerFactory("TutorGameDbAccessPU").createEntityManager();

    public Bundle add(Bundle bundle){
        em.getTransaction().begin();
        Bundle bundleFromDB = em.merge(bundle);
        em.getTransaction().commit();
        return bundleFromDB;
    }

    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    public Bundle get(long id){
        return em.find(Bundle.class, id);
    }

    public void update(Bundle bundle){
        em.getTransaction().begin();
        em.merge(bundle);
        em.getTransaction().commit();
    }

    public List<Bundle> getAll(){
        TypedQuery<Bundle> namedQuery = em.createNamedQuery("Bundle.getAll", Bundle.class);
        return namedQuery.getResultList();
    }
}
