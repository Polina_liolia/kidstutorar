﻿using KidsTutorAPI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using System.Web.Routing;

namespace KidsTutorAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/users
        [Authorize]
        [HttpGet]
        [ResponseType(typeof(IList<ApplicationUser>))]
        [Route("api/users")]
        public IHttpActionResult GetUsers()
        {
            List<ApplicationUser> users = db.Users.ToList();
            if (users == null)
            {
                return NotFound();
            }

            return Ok(new { results = users });

        }

        [Authorize]
        [HttpGet]
        [Route("api/user")]
        [ResponseType(typeof(ApplicationUser))]
        public IHttpActionResult GetApplicationUser()
        {
            //only authentificated user can send such request 
            string currentUserId = User.Identity.GetUserId();

            ApplicationUser applicationUser = db.Users.Include("Rewards").FirstOrDefault(u=>u.Id.Equals(currentUserId));
            if (applicationUser == null)
            {
                return NotFound();
            }

            return Ok(new { result = applicationUser });
        }

        [HttpGet]
        [ResponseType(typeof(IList<ApplicationUser>))]
        [Route("api/users/leaders")]
        public IHttpActionResult GetLeaders()
        {
            List<ApplicationUser> leaders = db.Users.OrderByDescending(u => u.Score).Take(10).ToList();
            if (leaders == null)
            {
                return NotFound();
            }
            return Ok(new { results = leaders });
        }

        [Authorize]
        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("api/users/set_name")]
        public IHttpActionResult SetName()
        {
            var json = Request.Content.ReadAsAsync<JObject>();
            string name = (string)Convert.ChangeType(json.Result["newName"], typeof(string)); 
            if(name == null)
            {
                return BadRequest();
            }
            
            //only authentificated user can send such request 
            string currentUserId = User.Identity.GetUserId();

            ApplicationUser user = db.Users.Find(currentUserId);

            db.Entry(user).State = EntityState.Modified;
            user.Name = name;
            db.SaveChanges();
            return StatusCode(HttpStatusCode.OK);
        }

        [Authorize]
        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("api/users/set_locale/{locale}")]
        public IHttpActionResult SetLocale(string locale)
        {
            //only authentificated user can send such request 
            string currentUserId = User.Identity.GetUserId();

            ApplicationUser user = db.Users.Find(currentUserId);

            db.Entry(user).State = EntityState.Modified;
            user.Locale = locale;
            db.SaveChanges();
            return StatusCode(HttpStatusCode.OK);
        }

        [Authorize]
        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("api/users/set_email")]
        public IHttpActionResult SetEmail()
        {
            var json = Request.Content.ReadAsAsync<JObject>();
            string email = (string)Convert.ChangeType(json.Result["email"], typeof(string));
            if (email == null)
            {
                return BadRequest();
            }

            //only authentificated user can send such request 
            string currentUserId = User.Identity.GetUserId();

            ApplicationUser user = db.Users.Find(currentUserId);

            db.Entry(user).State = EntityState.Modified;
            user.UserName = email;
            user.Email = email;
            db.SaveChanges();
            return StatusCode(HttpStatusCode.OK);
        }

        [Authorize]
        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("api/users/change_password")]
        public async Task<IHttpActionResult> ChangePassword()
        {
            var json = Request.Content.ReadAsAsync<JObject>();
            string oldPassword = (string)Convert.ChangeType(json.Result["oldPassword"], typeof(string));
            string newPassword = (string)Convert.ChangeType(json.Result["newPassword"], typeof(string));


            if (oldPassword == null || newPassword == null)
            {
                return BadRequest();
            }

            string currentUserId = User.Identity.GetUserId();
            ApplicationUser user = db.Users.Find(currentUserId);

            ApplicationUserManager userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            IdentityResult result = await userManager.ChangePasswordAsync(User.Identity.GetUserId(), oldPassword,
                newPassword);

            if (!result.Succeeded)
            {
                return BadRequest();
            }

            return Ok();
        }


        [Authorize]
        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("api/users/set_score/{score}")]
        public IHttpActionResult SetScore(int score)
        {
            //only authentificated user can send such request 
            string currentUserId = User.Identity.GetUserId();

            ApplicationUser user = db.Users.Find(currentUserId);

            if (score < 0 || user.Score > score)
            {
                return BadRequest("Score in DB is greater then passed one.");
            }
            db.Entry(user).State = EntityState.Modified;
            user.Score = score;
            db.SaveChanges();
            DefineReward(user);
            return StatusCode(HttpStatusCode.OK);
        }

        [Authorize]
        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("api/users/add_reward")]
        public IHttpActionResult AddReward()
        {
            //only authentificated user can send such request 
            string currentUserId = User.Identity.GetUserId();

            ApplicationUser user = db.Users.Find(currentUserId);
            //DefineReward(user);

            //calculating target number of rewards according to score
            long targetCount = (user.Score / 20) + 1;
            if (user.Rewards.Count < targetCount)
            {
                db.Entry(user).State = EntityState.Modified;
                List<Reward> rewards = db.Rewards.Where(r => r.Count <= targetCount).ToList();
                foreach (Reward r in rewards)
                {
                    user.Rewards.Add(r);
                }
                db.SaveChanges();
            }

            return StatusCode(HttpStatusCode.OK);
        }

        private void DefineReward(ApplicationUser user)
        {

        }

        [Authorize]
        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("api/users/set_complexity/{complexity}")]
        public IHttpActionResult SetComplexityLevel(int complexity)
        {
            //only authentificated user can send such request 
            string currentUserId = User.Identity.GetUserId();

            ApplicationUser user = db.Users.Find(currentUserId);

            if (complexity <= 0 || complexity > 3)
            {
                return BadRequest("Invalid complexity value");
            }

            if (complexity != user.ComplexityLevel)
            {
                db.Entry(user).State = EntityState.Modified;
                user.ComplexityLevel = complexity;
                db.SaveChanges();
            }

            return StatusCode(HttpStatusCode.OK);
        }

        [Authorize]
        [HttpPut]
        [ResponseType(typeof(void))]
        [Route("api/users/set_premium")]
        public IHttpActionResult SetPremium()
        {
            //only authentificated user can send such request 
            string currentUserId = User.Identity.GetUserId();
            ApplicationUser user = db.Users.Find(currentUserId);
            if(user == null)
            {
                return NotFound();
            }
            user.IsPremium = true;
            return Ok();
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ApplicationUserExists(string id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }
    }
}