package Helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.apache.commons.net.ftp.FTPClient;

public class BundleFtpLoader {

    JFrame parent;

    public BundleFtpLoader(JFrame parent) {
        this.parent = parent;
    }

    private File[] chooseFile() {
        File[] files = null;
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        int result = fileChooser.showOpenDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            files = fileChooser.getSelectedFiles();
        }
        return files;
    }

    public String uploadBundle(String ftpPath, String login, String password, String dir) {
        String assetName = "";
        FTPClient client = new FTPClient();
        FileInputStream fis = null;

        try {
            client.connect(ftpPath);
            client.login(login, password);

            //choose file
            File[] files = chooseFile();
            // Create an InputStream of the file to be uploaded
            for (File file : files) {
                fis = new FileInputStream(file);
                // Store file to server
                client.storeFile(dir + file.getName(), fis);
                assetName = file.getName().replaceFirst("[.][^.]+$", "");
            }
            client.logout();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(parent, "File uploading failed: " + e.getMessage(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                client.disconnect();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(parent, "FTP client disconnecting failed: " + e.getMessage(),
                        "Error", JOptionPane.ERROR_MESSAGE);
            }

        }
        return assetName;
    }
}
