﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class SoundsSource  {

    private Dictionary<string, Dictionary<string, List<string>>> clips;

    #region Singleton implementation
    public static SoundsSource Instance
    {
        get
        {
            return InstanceHolder.instance;
        }
    }

    private class InstanceHolder
    {
        static InstanceHolder()
        {

        }
        internal static readonly SoundsSource instance = new SoundsSource();
    }

    #endregion


    public SoundsSource()
    {
        clips = new Dictionary<string, Dictionary<string, List<string>>>();
        populateClips();
    }

    private void populateClips()
    {
        Dictionary<string, List<string>> ru = new Dictionary<string, List<string>>();
        Dictionary<string, List<string>> en = new Dictionary<string, List<string>>();
        
        clips.Add("ru-RU", ru);
        clips.Add("en-US", en);

        ru.Add("success", new List<string>()
        {
            "good_job_correct"
        });
        ru.Add("calculate", new List<string>()
        {
            "calculate_it"
        });
        ru.Add("plane", new List<string>()
        {
            "find_plane"
        });
        ru.Add("first_puzzle", new List<string>()
        {
            "first_puzzle"
        });
        ru.Add("kim", new List<string>()
        {
            "its_kim"
        });
        ru.Add("read", new List<string>()
        {
            "read_word"
        });
        ru.Add("oops", new List<string>()
        {
            "something_wrong"
        });
       
        ru.Add("wrong", new List<string>()
        {
            "wrong",
            "try_again"
        });

        //en
        en.Add("success", new List<string>()
        {
            "correct25",
            "goodjob25",
            "welldone25"
        });
        en.Add("calculate", new List<string>()
        {
            "solvethisproblem25"
        });
        en.Add("plane", new List<string>()
        {
            "hi25"
        });
        en.Add("first_puzzle", new List<string>()
        {
            "rewards25"
        });
        en.Add("kim", new List<string>()
        {
            "kimandboxes25"
        });
        en.Add("read", new List<string>()
        {
            "readthisword25"
        });
        en.Add("oops", new List<string>()
        {
            "oops25"
        });
        
        en.Add("wrong", new List<string>()
        {
            "wrong25"
        });

    }

    public AudioClip GetClip(string name, string locale)
    {
        AudioClip clip = null;
        Dictionary<string, List<string>> localeDictionary = null;
        clips.TryGetValue(locale, out localeDictionary);
        if(localeDictionary != null)
        {
            List<string> clipPathes = null;
            localeDictionary.TryGetValue(name, out clipPathes);
            if (clipPathes != null)
            {
                System.Random random = new System.Random();
                int rand = random.Next(0, clipPathes.Count);
                string path = clipPathes[rand];
                clip = Resources.Load<AudioClip>(path);
            }
            else
            {
                throw new KeyNotFoundException($"Clip {name} not found in {locale} dictionary");
            }
        }
        else
        {
            throw new KeyNotFoundException($"Locale not found: {locale}");
        }
        return clip;
    }
    
}
