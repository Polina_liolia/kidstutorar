﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShowSceneScript : MonoBehaviour {

   // public string nextSceneName;

    public void ShowScene(string nextSceneName)
    {
        Debug.Log("NextScene btn clicked");
        if (!nextSceneName.Equals(""))
        {
            SceneManager.LoadScene(sceneName: nextSceneName);
            Debug.Log($"NextScene: {nextSceneName}" );
        }
        else
        {
            Debug.Log("No scene name provided");
        }
    }
}
