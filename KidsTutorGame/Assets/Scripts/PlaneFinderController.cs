﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneFinderController : MonoBehaviour {

    public GameObject charBoy;
    public GameObject microphoneBtn;

    private CharAnimationsScript charAnimator;
    private AudioClip findPlaneClip;
    private AudioClip helloKimClip;
    private AudioSource audioData;



    private void Start()
    {
        StartGameScript.GamesCounter++;
        charAnimator = charBoy.GetComponent<CharAnimationsScript>();
        audioData = microphoneBtn.GetComponent<AudioSource>();

        string locale = ServerClientScript.User != null && ServerClientScript.User.Locale != null && ServerClientScript.User.Locale != string.Empty ?
           ServerClientScript.User.Locale : Application.systemLanguage == SystemLanguage.Russian ?
           "ru-RU" : "en-US";

        findPlaneClip = SoundsSource.Instance.GetClip("plane", locale);
        helloKimClip = SoundsSource.Instance.GetClip("kim", locale); ;

        if (StartGameScript.GamesCounter == 1) //game is launched for the first time
            audioData.PlayOneShot(findPlaneClip, 0.7F);

    }

    public void deactivate()
    {
        charAnimator.SayHallo();
        if (StartGameScript.GamesCounter == 1) //game is launched for the first time
        {
            audioData.Stop();
            audioData.PlayOneShot(helloKimClip, 0.7F);
        }
        this.gameObject.SetActive(false);
    }

    public void activate()
    {
        StartGameScript.GamesCounter++;
        this.gameObject.SetActive(true);
    }
}
