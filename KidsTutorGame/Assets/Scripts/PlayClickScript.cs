﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayClickScript : MonoBehaviour
{

    public void OnPlayBtnClicked()
    {
        if (ServerClientScript.User == null || ServerClientScript.User.ComplexityLevel <= 0)
        {
            SceneManager.LoadScene("SettingsScene");
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        else
        {
            SceneManager.LoadScene("MainScene");
        }
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "SettingsScene")
        {
            SceneManager.sceneLoaded -= OnSceneLoaded; //once
            List<GameObject> settingsSceneObjs = new List<GameObject>();
            scene.GetRootGameObjects(settingsSceneObjs);

            SettingsScript settingsScript = null;

            for (int i = 0; i < settingsSceneObjs.Count; i++)
            {
                var obj = settingsSceneObjs[i];
                if (obj.name == "SettingsMenu") //canvas
                {
                    Button[] btns = obj.GetComponentsInChildren<Button>();
                    for (int j = 0; j < btns.Length; j++)
                    {
                        if(btns[j].name == "btn_save")
                        {
                            settingsScript = btns[j].GetComponent<SettingsScript>();
                            settingsScript.playOnSave = true;
                            break;
                        }

                    }
                    break;
                }
            }
        }
    }
}
