﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[Serializable]
public class AuthData
{
    public string Token;
    public string Login;
    public string Password;
}

public class GameDataController : MonoBehaviour
{
    private static string authDataFileName = "my_auth_data.json";
    private static string userDataFileName = "my_user_data.json";

    public static AuthData AuthData
    {
        get
        {
            AuthData loadedData = null;
            string filePath = Path.Combine(Application.persistentDataPath, authDataFileName);

            if (File.Exists(filePath))
            {
                // Read the json from the file into a string
                string dataAsJson = File.ReadAllText(filePath);
                // Pass the json to JsonUtility, and tell it to create a GameData object from it
                loadedData = JsonUtility.FromJson<AuthData>(dataAsJson);
            }
            else
            {
                loadedData = new AuthData();
                string dataAsJson = JsonUtility.ToJson(loadedData);
                File.WriteAllText(filePath, dataAsJson);

                Debug.Log("Cannot load auth data from file! New file with empty data created");
            }
            return loadedData;
        }
        set
        {
            string dataAsJson = JsonUtility.ToJson(value);
            string filePath = Path.Combine(Application.persistentDataPath, authDataFileName);
            File.WriteAllText(filePath, dataAsJson);
        }
    }

    public static ApplicationUser User
    {
        get
        {
            ApplicationUser user = new ApplicationUser();
            string filePath = Path.Combine(Application.persistentDataPath, userDataFileName);

            if (File.Exists(filePath))
            {
                // Read the json from the file into a string
                string dataAsJson = File.ReadAllText(filePath);
                // Pass the json to JsonUtility, and tell it to create a GameData object from it
                user = JsonUtility.FromJson<ApplicationUser>(dataAsJson);
            }
            else
            {
                user = new ApplicationUser();
                string dataAsJson = JsonUtility.ToJson(user);
                File.WriteAllText(filePath, dataAsJson);
                Debug.Log("Cannot load user data from file! New file with empty data created");
            }
            return user;
        }
        set
        {
            string dataAsJson = JsonUtility.ToJson(value);
            string filePath = Path.Combine(Application.persistentDataPath, userDataFileName);
            File.WriteAllText(filePath, dataAsJson);
        }

    }


}
