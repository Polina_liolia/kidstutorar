﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharAnimationsScript : MonoBehaviour {

    Animator animator;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}

    public void Idle()
    {
        animator.SetTrigger("IdleTrigger");
    }

    public void Happy()
    {
        animator.SetTrigger("HappyTrigger");
    }

    public void Sad()
    {
        animator.SetTrigger("SadTrigger");
    }

    public void Thoughtfull()
    {
        animator.SetTrigger("ThinkingTrigger");
    }

    public void SayHallo()
    {
        animator.SetTrigger("SayHelloTrigger");
    }

    public void PointOnModel()
    {
        animator.SetTrigger("PointingTrigger");
    }

    public void HandleAction(string action)
    {
        switch (action)
        {
            case "boxing":
                {
                    Box();
                }break;
            case "jumping":
                {
                    Jump();
                }
                break;
            case "samba_dancing":
                {
                    Dance();
                }
                break;
            case "martelo_do_chau":
                {
                    ShowTrick();
                }
                break;
        }
    }

    public void Jump()
    {
        animator.SetTrigger("JumpingTrigger");
    }

    public void Dance()
    {
        animator.SetTrigger("DancingTrigger");
    }

    public void ShowTrick()
    {
        animator.SetTrigger("TrickTrigger");
    }

    public void Box()
    {
        animator.SetTrigger("BoxingTrigger");
    }


}
