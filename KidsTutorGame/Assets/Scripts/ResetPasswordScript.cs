﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResetPasswordScript : MonoBehaviour {

    public Text textField;
    public GameObject Preloader;
    public InputField EmailInput;
    public GameObject SuccessPopup;
    private string locale;
    private string email;
    private string message = "";

    private void Start()
    {
        locale = ServerClientScript.User != null && ServerClientScript.User.Locale != null && ServerClientScript.User.Locale != string.Empty ?
           ServerClientScript.User.Locale : Application.systemLanguage == SystemLanguage.Russian ?
           "ru-RU" : "en-US";

        Preloader.SetActive(false);
        SuccessPopup.SetActive(false);

        EmailInput.onEndEdit.AddListener(delegate { OnEmailInputEditEnd(EmailInput); });
    }

    void OnGUI()
    {
        textField.text = message;
    }

    public void OnEmailInputEditEnd(InputField input)
    {
        email = input.text;
    }

    public void OnResetPasswordBtnClicked()
    {
        if (!InputValidator.IsEmailValid(email))
        {
            string msg = locale.Equals("ru-RU") ? "Введите правильный email" : "Wrong email";
            message = $"{message}{Environment.NewLine}{msg}";
            return;
        }
        StartCoroutine(resetPassword());
    }

    private IEnumerator resetPassword()
    {
        Preloader.SetActive(true);
        //get bundles
        yield return StartCoroutine(ServerClientScript.ResetPassword(email));
        Preloader.SetActive(false);
        SuccessPopup.SetActive(true);
    }
}
