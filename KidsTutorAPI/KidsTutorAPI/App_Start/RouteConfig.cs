﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace KidsTutorAPI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "ConfirmEmailSuccess",
               url: "Password/ConfirmEmailSuccess",
               defaults: new { controller = "Password", action = "ConfirmEmailSuccess" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Api",
                url: "api/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

           // routes.MapRoute(
           //    name: "ConfirmEmailRoute",
           //    url: "api/{controller}/{action}/{userid}/{code}",
           //    defaults: new { controller = "Account", action = "ConfirmEmail", id = UrlParameter.Optional, code = UrlParameter.Optional }
           //);

            routes.MapRoute(
               name: "GameResource",
               url: "{controller}/{action}/{complexity}/{count}",
               defaults: new { controller = "Bundles", action = "Bundles", count = UrlParameter.Optional }
           );
        }
    }
}
