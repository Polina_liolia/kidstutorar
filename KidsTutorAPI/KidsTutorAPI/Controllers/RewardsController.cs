﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using KidsTutorAPI.Models;

namespace KidsTutorAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RewardsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Rewards
        public IQueryable<Reward> GetRewards()
        {
            return db.Rewards;
        }

        // GET: api/Rewards/5
        [ResponseType(typeof(Reward))]
        [Route("api/rewards/{count}")]
        public IHttpActionResult GetReward(int count)
        {
            Reward reward = db.Rewards.Where(r => r.Count == count).FirstOrDefault();
            if (reward == null)
            {
                return NotFound();
            }

            return Ok(reward);
        }

        [ResponseType(typeof(IList<Reward>))]
        [Route("api/rewards/current/{score}")]
        public IHttpActionResult GetCurrentRewards(int score)
        {
            long targetCount = (score / 20) + 1;
            List<Reward> rewards = db.Rewards.Where(r => r.Count <= targetCount).ToList();

            if (rewards == null)
            {
                return NotFound();
            }

            return Ok(new { results = rewards });
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RewardExists(int id)
        {
            return db.Rewards.Count(e => e.Id == id) > 0;
        }
    }
}