﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using KidsTutorAPI.Models;
using Microsoft.AspNet.Identity;

namespace KidsTutorAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BundlesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        
        // GET: api/Bundles/5
        [ResponseType(typeof(IList<Bundle>))]
        [Route("api/bundles/{locale}/{complexity}/{count}/{isFree}")]
        public IHttpActionResult GetBundles(string locale, int complexity, int? count, string isFree)
        {
            //default values:
            if (count.Equals(null))
                count = 5;
            if (isFree.Equals(string.Empty))
                isFree = "true";

            IList<Bundle> bundles = null;
            if (isFree.ToLower().Equals("true"))
            {
                bundles = (from bundle in db.Bundles
                           where bundle.IsFree &&
                           bundle.Words.AsQueryable()
                           .FirstOrDefault(w => w.Locale.Equals(locale) && w.ComplexityLevel <= complexity) != null
                           orderby Guid.NewGuid()
                           select bundle)
                           .Take(count.Value)
                           .ToList();           
            }
            else
            {
                bundles = (from bundle in db.Bundles
                           where bundle.Words.AsQueryable()
                           .FirstOrDefault(w => w.Locale.Equals(locale) && w.ComplexityLevel <= complexity) != null
                           orderby Guid.NewGuid()
                           select bundle)
                           .Take(count.Value)
                           .ToList();
            }

            if (bundles == null)
            {
                return NotFound();
            }
            return Ok(new { results = bundles });
        }



       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BundleExists(int id)
        {
            return db.Bundles.Count(e => e.Id == id) > 0;
        }
    }
}