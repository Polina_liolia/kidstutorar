﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MathScript : MonoBehaviour {

    public Button answerBtn;
    public InputField inputAnswer;

    public AudioClip LoseSound;
    private AudioSource audioData;

    public GameObject charBoy;
    private CharAnimationsScript charAnimator;

    // Use this for initialization
    void Start () {
        //initialise audio source:
        audioData = GetComponent<AudioSource>();
        charAnimator = charBoy.GetComponent<CharAnimationsScript>();
    }
	
	// Update is called once per frame
	void Update () {
        if (StatesScript.State != GameState.math)
        {
            answerBtn.GetComponent<Button>().interactable = false;
        }
        else if(!inputAnswer.text.Equals(string.Empty))
        {
            answerBtn.GetComponent<Button>().interactable = true;
        }
    }

    public void OnAnswerBtnClick()
    {
        int answer = -1; 
        System.Int32.TryParse(inputAnswer.text, out answer);
        if (answer == ObjectsScript.CurrentMathData.Result) //success
        {
            Debug.Log("Correct!");
            StatesScript.State = GameState.model; //model state
            StatesScript.CurrentResult = true;
            StatesScript.IncrementScore = true;
        }
        else //wron answer
        {
            Debug.Log(string.Format("Wrong calc"));
            StatesScript.TriesCounter++; //play lose sound
            if (StatesScript.TriesCounter > 0)
            {
                audioData.PlayOneShot(LoseSound, 0.7F);
                charAnimator.Sad(); //char's reaction
            }
            if (StatesScript.TriesCounter >= 3)
            {
                StatesScript.State = GameState.mystery; //mystery state
                charAnimator.Idle(); //char's reaction
            }
        }

        inputAnswer.text = "";
    }
}
