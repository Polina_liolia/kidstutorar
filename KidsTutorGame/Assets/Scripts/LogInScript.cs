﻿using UnityEngine;
using UnityEngine.UI;
using ServerClientLib;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class LogInScript : MonoBehaviour {

    public Text textField;
    public InputField EmailInput;
    public InputField PasswordInput;
    public GameObject Preloader;

    private string email = "polina.liolia@gmail.com";
    private string password = "123456";
    private string message = "";

    private string locale;

    Server server = new Server(ServerClientScript.Host);


    private void Start()
    {
        locale = ServerClientScript.User != null && ServerClientScript.User.Locale != null && ServerClientScript.User.Locale != string.Empty ?
           ServerClientScript.User.Locale : Application.systemLanguage == SystemLanguage.Russian ?
           "ru-RU" : "en-US";
        Preloader.SetActive(false);
        EmailInput.onEndEdit.AddListener(delegate { OnEmailInputEditEnd(EmailInput); });
        PasswordInput.onEndEdit.AddListener(delegate { OnPasswordInputEditEnd(PasswordInput); });
    }

    void OnGUI()
    {
        textField.text = message;
    }

    public void OnEmailInputEditEnd(InputField input)
    {
        email = input.text;
    }

    public void OnPasswordInputEditEnd(InputField input)
    {
        password = input.text;
    }


    public void OnLogin()
    {
        if (!InputValidator.IsEmailValid(email))
        {
            string msg = locale.Equals("ru-RU") ? "Введите правильный email" : "Wrong email";
            message = $"{message}{Environment.NewLine}{msg}";
            return;
        }
        StartCoroutine(LogIn(email, password));
    }

    public IEnumerator LogIn(string email, string password)
    {
        Debug.Log("Log In action...");
        Preloader.SetActive(true);

        yield return ServerClientScript.GetToken(email, password);

        if (ServerClientScript.IsSignedIn && ServerClientScript.UserToken != null && !ServerClientScript.UserToken.Equals(string.Empty)) //succeeded
        { 

            yield return StartCoroutine(ServerClientScript.GetUser());

            if (ServerClientScript.User.Id != null) //succeeded
            {
                GameDataController.User = ServerClientScript.User;
            }
            else //failed
            {
                ServerClientScript.IsSignedIn = false;
                ServerClientScript.User = new ApplicationUser();
                GameDataController.AuthData = new AuthData();
            }

            SceneManager.LoadScene(sceneName: "MenuScene");
        }
        else //failed
        {
            ServerClientScript.IsSignedIn = false;
            ServerClientScript.User = new ApplicationUser();
            GameDataController.AuthData = new AuthData();

            Debug.Log("Can't get token");
            string msg = locale.Equals("ru-RU") ? "Ошибка авторизации" : "Sing in error";
            message = $"{message}{Environment.NewLine}{msg}";
        }
        Preloader.SetActive(false);
    }
}
