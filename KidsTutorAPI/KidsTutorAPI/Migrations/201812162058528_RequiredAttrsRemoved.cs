namespace KidsTutorAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequiredAttrsRemoved : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "ComplexityLevel", c => c.Int(nullable: true, defaultValue: 1));
            AlterColumn("dbo.AspNetUsers", "Score", c => c.Long(nullable: true, defaultValue: 0));
            AlterColumn("dbo.AspNetUsers", "HelpAvailable", c => c.Int(nullable: true, defaultValue: 5));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "ComplexityLevel", c => c.Int(nullable: false));
            AlterColumn("dbo.AspNetUsers", "Score", c => c.Long(nullable: false));
            AlterColumn("dbo.AspNetUsers", "HelpAvailable", c => c.Int(nullable: false));
        }
    }
}
