﻿using System;

namespace TutorLib
{
    [Serializable]
    public class Reward
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public int Count { get; set; }
    }
}