﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KidsTutor.Models
{
    public class CategoryUser
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public CategoryUser()
        {

        }

        public CategoryUser(int userId, int categoryId)
        {
            UserId = userId;
            CategoryId = categoryId;
        }
    }
}
