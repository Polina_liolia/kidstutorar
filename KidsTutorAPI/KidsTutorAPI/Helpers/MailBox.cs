﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Configuration;

namespace KidsTutorAPI.Helpers
{
    public class MailBox
    {
        public void SendMail(string email, string subject, string msg)
        {
            string FROM = ConfigurationManager.AppSettings["mailFrom"];
            string FROMNAME = ConfigurationManager.AppSettings["mailSenderName"];
            string SMTP_USERNAME = ConfigurationManager.AppSettings["smtpUser"];
            string SMTP_PASSWORD = ConfigurationManager.AppSettings["smtpPassword"];
            string HOST = ConfigurationManager.AppSettings["smtpHost"];

            string TO = email;
            int PORT = 587;

            // The subject line of the email
            String SUBJECT = subject;

            // The body of the email
            String BODY = msg;

            // Create and build a new MailMessage object
            MailMessage message = new MailMessage();
            message.IsBodyHtml = false;
            message.From = new MailAddress(FROM, FROMNAME);
            message.To.Add(new MailAddress(TO));
            message.Subject = SUBJECT;
            message.Body = BODY;
           
            using (var client = new System.Net.Mail.SmtpClient(HOST, PORT))
            {
                // Pass SMTP credentials
                client.Credentials =
                    new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);

                // Enable SSL encryption
                client.EnableSsl = true;

                // Try to send the message. Show status in console.
                try
                {
                    Console.WriteLine("Attempting to send email...");
                    client.Send(message);
                    Console.WriteLine("Email sent!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("The email was not sent.");
                    Console.WriteLine("Error message: " + ex.Message);
                }
            }
        }
    }
}