﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeComplexityScript : MonoBehaviour {

    public Button BtnComplexity1;
    public Button BtnComplexity2;
    public Button BtnComplexity3;

    private void setAllComplexityEnabled()
    {
        BtnComplexity1.interactable = true;
        BtnComplexity2.interactable = true;
        BtnComplexity3.interactable = true;
    }

    public void setComplexityDisabled(int complexity)
    {
        setAllComplexityEnabled();
        switch (complexity)
        {
            case 1:
                BtnComplexity1.interactable = false;
                break;
            case 2:
                BtnComplexity2.interactable = false;
                break;
            case 3:
                BtnComplexity3.interactable = false;
                break;
        }
    }
}
