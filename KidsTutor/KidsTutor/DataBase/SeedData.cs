﻿using KidsTutor.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KidsTutor.DataBase
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<GameDbContext>();
            context.Database.EnsureCreated();

            if (!context.Categories.Any())
            {
                context.Categories.AddRange(new Category[]{
                    new Category("Nature"),
                    new Category("Food"),
                    new Category("Transport"),
                    new Category("Other"),
                    new Category("SuccessSound"),
                    new Category("LooseSound"),
                    new Category("MysteryBox")
                });
                context.SaveChanges();
            }


            string ftpRoot = @"ftp://polinaliolia:drivehqgjkbyf12@ftp.drivehq.com/assets/";

            if (!context.Bundles.Any())
            {
                context.Bundles.AddRange(new Bundle[]
                {
                    new Bundle("mystery box", String.Format("{0}{1}", ftpRoot, "mystery_box"), "box_fbx", "-", 1),

                    //nature
                    new Bundle("fish", String.Format("{0}{1}", ftpRoot, "fish"), "Z3_fbx", "ры-ба", 1),
                    new Bundle("rose", String.Format("{0}{1}", ftpRoot, "rose"), "rose", "ро-за", 1),
                    new Bundle("snake", String.Format("{0}{1}", ftpRoot, "snake"), "Snake", "зме-я", 1),
                    new Bundle("tree", String.Format("{0}{1}", ftpRoot, "tree"), "Tree_FBX", "де-ре-во", 1),
                    new Bundle("dino", String.Format("{0}{1}", ftpRoot, "dino"), "TrexByJoel3d", "ав-то-мо-биль", 2),

                    //food
                    new Bundle("ananas", String.Format("{0}{1}", ftpRoot, "ananas"), "ananas", "а-на-нас", 2),
                    new Bundle("apple", String.Format("{0}{1}", ftpRoot, "apple"), "apple textured obj", "яб-ло-ко", 2),
                    new Bundle("banana", String.Format("{0}{1}", ftpRoot, "banana"), "Banana", "ба-нан", 1),
                    new Bundle("orange", String.Format("{0}{1}", ftpRoot, "orange"), "Orange", "а-пель-син", 2),

                    //transport
                    new Bundle("plane", String.Format("{0}{1}", ftpRoot, "plane"), "B_787_8", "са-мо-лёт", 2),
                    new Bundle("motocycle", String.Format("{0}{1}", ftpRoot, "motocycle"), "BSA_BantamD1_BRAZIL_OBJ", "мо-то-цикл", 3),
                    new Bundle("boat", String.Format("{0}{1}", ftpRoot, "boat"), "OldBoat", "ло-дка", 2),
                    new Bundle("car", String.Format("{0}{1}", ftpRoot, "car"), "Z3_fbx", "ав-то-мо-биль", 3),

                    //other
                    new Bundle("basketball", String.Format("{0}{1}", ftpRoot, "basketball"), "basketball", "мяч", 1),
                    new Bundle("converse", String.Format("{0}{1}", ftpRoot, "converse"), "converse_fbx", "ке-ды", 1),
                    new Bundle("oven", String.Format("{0}{1}", ftpRoot, "oven"), "DB_Apps&Tech_04_13", "печь", 1),
                    new Bundle("house", String.Format("{0}{1}", ftpRoot, "house"), "house_obj", "дом", 1),
                    new Bundle("pencil", String.Format("{0}{1}", ftpRoot, "pencil"), "pencil", "ка-ран-даш", 3),
                    new Bundle("chest", String.Format("{0}{1}", ftpRoot, "chest"), "treasure_chest", "сун-дук", 2),

                    //success sounds
                    new Bundle("applause", String.Format("{0}{1}", ftpRoot, "applause"), "applause", "", 1),
                    new Bundle("applause_whistles", String.Format("{0}{1}", ftpRoot, "applause_whistles"), "applause_whistles", "", 1),
                    new Bundle("oh_yeh_soccer", String.Format("{0}{1}", ftpRoot, "oh_yeh_soccer"), "oh_yeh_soccer", "", 1),
                    new Bundle("success_robot", String.Format("{0}{1}", ftpRoot, "success_robot"), "success_robot", "", 1),
                    new Bundle("win_magic", String.Format("{0}{1}", ftpRoot, "win_magic"), "win_magic", "", 1),

                    //loose sounds
                    new Bundle("oh_no_cartoon", String.Format("{0}{1}", ftpRoot, "oh_no_cartoon"), "oh_no_cartoon", "", 1),
                    new Bundle("oh_no_funny", String.Format("{0}{1}", ftpRoot, "oh_no_funny"), "oh_no_funny", "", 1),
                    new Bundle("oh_no_toddler", String.Format("{0}{1}", ftpRoot, "oh_no_toddler"), "oh_no_toddler", "", 1),
                });
                context.SaveChanges();
            }

            if (!context.BundleCategories.Any())
            {
                context.BundleCategories.AddRange(new BundleCategory[]
            {
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "mystery box").Id, context.Categories.FirstOrDefault(c => c.Name == "MysteryBox").Id),

                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "fish").Id, context.Categories.FirstOrDefault(c => c.Name == "Nature").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "rose").Id, context.Categories.FirstOrDefault(c => c.Name == "Nature").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "snake").Id, context.Categories.FirstOrDefault(c => c.Name == "Nature").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "tree").Id, context.Categories.FirstOrDefault(c => c.Name == "Nature").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "dino").Id, context.Categories.FirstOrDefault(c => c.Name == "Nature").Id),

                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "ananas").Id, context.Categories.FirstOrDefault(c => c.Name == "Food").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "apple").Id, context.Categories.FirstOrDefault(c => c.Name == "Food").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "banana").Id, context.Categories.FirstOrDefault(c => c.Name == "Food").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "orange").Id, context.Categories.FirstOrDefault(c => c.Name == "Food").Id),

                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "plane").Id, context.Categories.FirstOrDefault(c => c.Name == "Transport").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "motocycle").Id, context.Categories.FirstOrDefault(c => c.Name == "Transport").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "boat").Id, context.Categories.FirstOrDefault(c => c.Name == "Transport").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "car").Id, context.Categories.FirstOrDefault(c => c.Name == "Transport").Id),

                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "basketball").Id, context.Categories.FirstOrDefault(c => c.Name == "Other").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "converse").Id, context.Categories.FirstOrDefault(c => c.Name == "Other").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "oven").Id, context.Categories.FirstOrDefault(c => c.Name == "Other").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "house").Id, context.Categories.FirstOrDefault(c => c.Name == "Other").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "pencil").Id, context.Categories.FirstOrDefault(c => c.Name == "Other").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "chest").Id, context.Categories.FirstOrDefault(c => c.Name == "Other").Id),

                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "applause").Id, context.Categories.FirstOrDefault(c => c.Name == "SuccessSound").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "applause_whistles").Id, context.Categories.FirstOrDefault(c => c.Name == "SuccessSound").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "oh_yeh_soccer").Id, context.Categories.FirstOrDefault(c => c.Name == "SuccessSound").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "success_robot").Id, context.Categories.FirstOrDefault(c => c.Name == "SuccessSound").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "win_magic").Id, context.Categories.FirstOrDefault(c => c.Name == "SuccessSound").Id),

                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "oh_no_cartoon").Id, context.Categories.FirstOrDefault(c => c.Name == "LooseSound").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "oh_no_funny").Id, context.Categories.FirstOrDefault(c => c.Name == "LooseSound").Id),
                    new BundleCategory(context.Bundles.FirstOrDefault(b => b.Name == "oh_no_toddler").Id, context.Categories.FirstOrDefault(c => c.Name == "LooseSound").Id),

            });
                context.SaveChanges();
            }

            if (!context.Users.Any())
            {
                context.Users.AddRange(new User[]
                {
                    new User("small_user", new DateTime(2012, 7, 8)),
                    new User("big_user", new DateTime(2008, 9, 12), 3, 0, 5)
                });
                context.SaveChanges();
            }

            if (!context.CategoryUsers.Any())
            {
                context.CategoryUsers.AddRange(new CategoryUser[]
                {
                    new CategoryUser(context.Users.FirstOrDefault(u => u.Name == "small_user").Id, context.Categories.FirstOrDefault(c => c.Name == "Nature").Id),
                    new CategoryUser(context.Users.FirstOrDefault(u => u.Name == "small_user").Id, context.Categories.FirstOrDefault(c => c.Name == "Food").Id),
                    new CategoryUser(context.Users.FirstOrDefault(u => u.Name == "small_user").Id, context.Categories.FirstOrDefault(c => c.Name == "Transport").Id),

                    new CategoryUser(context.Users.FirstOrDefault(u => u.Name == "big_user").Id, context.Categories.FirstOrDefault(c => c.Name == "Other").Id)
                });
            }
            context.SaveChanges();
        }
    }
}

