namespace KidsTutorAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ComplexityLevelDefaultMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "ComplexityLevel", c => c.Int(nullable: false, defaultValue: 1));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "ComplexityLevel", c => c.Int(nullable: false));
        }
    }
}
