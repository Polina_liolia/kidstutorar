namespace KidsTutorAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RewardAddUser : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Rewards", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Rewards", new[] { "ApplicationUser_Id" });
            CreateTable(
                "dbo.ApplicationUserRewards",
                c => new
                    {
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                        Reward_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ApplicationUser_Id, t.Reward_Id })
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id, cascadeDelete: true)
                .ForeignKey("dbo.Rewards", t => t.Reward_Id, cascadeDelete: true)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.Reward_Id);
            
            DropColumn("dbo.Rewards", "ApplicationUser_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Rewards", "ApplicationUser_Id", c => c.String(maxLength: 128));
            DropForeignKey("dbo.ApplicationUserRewards", "Reward_Id", "dbo.Rewards");
            DropForeignKey("dbo.ApplicationUserRewards", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ApplicationUserRewards", new[] { "Reward_Id" });
            DropIndex("dbo.ApplicationUserRewards", new[] { "ApplicationUser_Id" });
            DropTable("dbo.ApplicationUserRewards");
            CreateIndex("dbo.Rewards", "ApplicationUser_Id");
            AddForeignKey("dbo.Rewards", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
