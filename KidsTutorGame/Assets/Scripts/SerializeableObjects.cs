﻿using System;

[Serializable]
public class BundleJsonRoot
{
    public Bundle[] results;
}

[Serializable]
public class ApplicationUserJsonRoot
{
    public ApplicationUser[] results;
}

[Serializable]
public class RewardJsonRoot
{
    public Reward[] results;
}

[Serializable]
public class MathJsonRoot
{
    public Calculation[] results;
}

[Serializable]
public class ApplicationUserJsonRootSingle
{
    public ApplicationUser result;
}

[Serializable]
public class LocaleJsonRoot
{
    public WordLocalizationData[] root;
}

[Serializable]
public class WordLocalizationData
{
    public string key;
    public Translation[] translations;
}

[Serializable]
public class Translation
{
    public string locale;
    public string value;
}

[Serializable]
public class Bundle
{
    public int Id;
    public string Name;
    public string Path;
    public string AssetName;
    public bool IsAction;
    public bool IsFree;

    public Category[] Categories;
    public Word[] Words;
}

[Serializable]
public class Word
{
    public int Id;
    public string Locale;
    public string Text;
    public int ComplexityLevel;
    public Bundle Bundle;
}

[Serializable]
public class Category
{
    public int Id;
    public string Name;
    public string Description;

    public ApplicationUser[] Users;
    public Bundle[] Bundles;
}

[Serializable]
public class Reward
{
    public int Id;
    public string Name;
    public string Url;
    public int Count;
}

[Serializable]
public class ApplicationUser
{
    public string Id;
    public string Name;
    public string UserName;
    public string Email;
    public int ComplexityLevel;
    public long Score;
    public string Locale;
    public bool IsPremium;
    public Reward[] Rewards;
}

[Serializable]
public class LoginData
{
    public string access_token;
    public string token_type;
    public long expires_in;
    public string userName;
    public DateTime issued;
    public DateTime expires;

}

[Serializable]
public class Calculation
{
    public int Id;
    public int FirstPart;
    public int SecondPart;
    public string Sign;
    public int Result;
}

