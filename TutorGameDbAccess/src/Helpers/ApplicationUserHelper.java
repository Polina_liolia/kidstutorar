package Helpers;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import tutorgamedbaccess.ApplicationUser;

public class ApplicationUserHelper {

    public EntityManager em = Persistence.createEntityManagerFactory("TutorGameDbAccessPU").createEntityManager();

    public ApplicationUser add(ApplicationUser reward){
        em.getTransaction().begin();
        ApplicationUser rewardFromDB = em.merge(reward);
        em.getTransaction().commit();
        return rewardFromDB;
    }

    public void delete(String id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    public ApplicationUser get(String id){
        return em.find(ApplicationUser.class, id);
    }

    public void update(ApplicationUser reward){
        em.getTransaction().begin();
        em.merge(reward);
        em.getTransaction().commit();
    }

    public List<ApplicationUser> getAll(){
        TypedQuery<ApplicationUser> namedQuery = em.createNamedQuery("ApplicationUser.getAll", ApplicationUser.class);
        return namedQuery.getResultList();
    }
}
