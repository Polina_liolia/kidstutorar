﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class RewardsScript : MonoBehaviour
{

    public Image puzzle1;
    public Image puzzle2;
    public Image puzzle3;
    public Image puzzle4;

    public GameObject parent;
    public GameObject Preloader;

    private readonly string ftpPath = @"ftp://ftp.drivehq.com/";
    private readonly string userName = "polinaliolia";
    private readonly string password = "drivehqgjkbyf12";

    // Use this for initialization
    void Start()
    {
        StartCoroutine(CreateRewards());
    }

    private IEnumerator CreateRewards()
    {
        //if (ServerClientScript.IsSignedIn)
        //{
        //    yield return StartCoroutine(ServerClientScript.UpdateRewards());
        //    yield return StartCoroutine(ServerClientScript.GetUser());
        //}
        //else
        //{
        long score = ServerClientScript.User != null ? ServerClientScript.User.Score : 0;
        yield return StartCoroutine(ServerClientScript.GetCurrentRewards(score));
        //}
        //now rewards of ServerClientScript.User are updated, iterating:

        for (int i = 0; i < ServerClientScript.User.Rewards.Length; i++)
        {
            Reward reward = ServerClientScript.User.Rewards[i];
            int partsHidden = 0;
            if (i == ServerClientScript.User.Rewards.Length - 1) //the last reward
            {
                long uncompletedScore = ServerClientScript.User.Score % 20;
                partsHidden = uncompletedScore < 5 ? 4 :
                    uncompletedScore < 10 ? 3 :
                    uncompletedScore < 15 ? 2 :
                    uncompletedScore < 20 ? 1 : 0;
            }

            string filePath = $"{reward.Url}";
            string fileName = filePath.Replace("pics/", "");

            RewardsFileExtraData rewardData = new RewardsFileExtraData()
            {
                Host = ftpPath,
                Password = password,
                UserName = userName,
                Count = reward.Count,
                FileName = fileName,
                FilePath = filePath,
                Parent = parent,
                PartsHidden = partsHidden,
                Puzzle = partsHidden == 4 ? puzzle4 : partsHidden == 3 ? puzzle3 : partsHidden == 2 ? puzzle2 : partsHidden == 1 ? puzzle1 : null
            };
            RewardsAsyncLoader loader = new RewardsAsyncLoader(rewardData);


            if(i == ServerClientScript.User.Rewards.Length - 1)//last file
            {
                loader.Client.DownloadFileCompleted += (object sender, AsyncCompletedEventArgs e) => {
                    Preloader.SetActive(false);
                };
            }

            loader.LoadAsync();
            //DownloadFile(reward, i, partsHidden, ftpPath, userName, password);
        }
    }
}
