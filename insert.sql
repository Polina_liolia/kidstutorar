use [tutorgamedb];

--delete from  [dbo].[Bundles] where [IsAction] = 0;
--delete from [dbo].[Words] where [Id] < 17 OR [Id] > 24;
--select * from [dbo].[Bundles];
--select * from [dbo].[Words];
--go
--
--ALTER PROCEDURE [dbo].[insert_bundle]
--( @bundle_name  AS varchar(30),
--@asset_name AS varchar(30),
--@word_ru AS nvarchar(30),
--@complexity_ru AS int,
--@word_en AS varchar(30),
--@complexity_en AS int,
--@is_action AS bit = 0,
--@is_free AS bit = 1     
--)    
--AS
--    BEGIN   
--        INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES(@bundle_name, @bundle_name, @asset_name, @is_action, @is_free);
--		INSERT INTO [dbo].[Words] ([Locale], [Text], [ComplexityLevel], [Bundle_Id]) VALUES ('ru-RU', @word_ru, @complexity_ru, IDENT_CURRENT('dbo.Bundles'));
--		INSERT INTO [dbo].[Words] ([Locale], [Text], [ComplexityLevel], [Bundle_Id]) VALUES ('en-US', @word_en, @complexity_en, IDENT_CURRENT('dbo.Bundles'));
--	END;
--
--go
--
--exec [dbo].[insert_bundle] 'ananas', 'ananas', N'������', 1, 'pineapple', 3;

--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('ananas', 'ananas', 'ananas', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('apple', 'apple', 'apple textured obj', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('ball', 'ball', 'basketball', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('banana', 'banana', 'Banana', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('barrel', 'barrel', 'barrel', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('birch_tree', 'birch_tree', 'Tree_FBX', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('bizon', 'bizon', 'bison_low', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('boat', 'boat', 'OldBoat', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('bulldog', 'bulldog', 'EnglishBulldog_by_AlexLashko_FreeSample', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('butterfly', 'butterfly', 'batterfly_yellow', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('cactus', 'cactus', 'cactus', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('can_peaches', 'can_peaches', 'can_peaches', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('car', 'car', 'Lamborghini_Aventador', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('cat', 'cat', 'cat_01_color05', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('chicken', 'chicken', 'chicken_01', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('christmas_tree', 'christmas_tree', 'lowtree', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('converse', 'converse', 'converse_fbx', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('deer', 'deer', 'deer', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('delfin', 'delfin', 'delfin_low', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('dice', 'dice', 'Dice', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('dino', 'dino', 'TrexByJoel3d', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('doll', 'doll', 'Doll', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('dummy', 'dummy', 'dummy_obj', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('earth', 'earth', 'Earth', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('ewer', 'ewer', 'EwerOBJ', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('fish', 'fish', 'GoldenFish', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('frog', 'frog', 'Frog_papermodel', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('glasses', 'glasses', 'Glasses', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('guitar', 'guitar', 'guitar_low', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('hare', 'hare', 'hare', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('headphone', 'headphone', 'headphone', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('heart', 'heart', 'heart_lp', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('horse', 'horse', 'Hourse_FBX', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('house', 'house', 'house_obj', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('kupina', 'kupina', 'Kupina', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('locomotive', 'locomotive', 'Locomotive', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('loose_sounds', 'loose_sounds', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('lotus', 'lotus', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('moto', 'moto', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('mystery_box', 'mystery_box', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('orange', 'orange', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('oven', 'oven', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('palm', 'palm', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('pencil', 'pencil', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('plain', 'plain', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('plane', 'plane', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('rhino', 'rhino', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('rose', 'rose', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('skull', 'skull', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('sled', 'sled', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('spruce', 'spruce', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('success_sounds', 'success_sounds', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('treasure', 'treasure', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('tree', 'tree', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('windmill', 'windmill', '', 0, 1);
--INSERT INTO [dbo].[Bundles] ([Name], [Path], [AssetName], [IsAction], [IsFree]) VALUES('wolf', 'wolf', '', 0, 1);
