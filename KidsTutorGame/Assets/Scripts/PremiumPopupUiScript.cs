﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PremiumPopupUiScript : MonoBehaviour {

    public Button GetPremiumBtn;
    public Button RestorePremiumBtn;
    public GameObject ErrorPopup; 
    public GameObject Preloader;

	// Use this for initialization
	void Start () {
        ErrorPopup.SetActive(false);
        Preloader.SetActive(false);
        if (ServerClientScript.User.IsPremium)
        {
            GetPremiumBtn.enabled = false;
            RestorePremiumBtn.enabled = false;
        }

        if(Application.platform == RuntimePlatform.Android)
        {
            RestorePremiumBtn.enabled = false;
        }

    }
}
