﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Net;
using System.IO;




public class ObjectsScript : MonoBehaviour
{
    public GameObject Word;
    public GameObject MathObject;
    public GameObject ScoreObj;
    public GameObject mysteryBoxParent;
    public GameObject modelsParent;
    public GameObject Preloader;
    public Transform mysteryBoxPrefab;
    public Transform Parent;
    public GameObject PremiumPopup;

    public static bool isIos = Application.platform == RuntimePlatform.IPhonePlayer;
    public static bool ModelHasToBeLoaded = false;

    public GameObject effectObject;
    public GameObject mathSuccessObject;

    public ModelObject model3DObject;
    public Transform mysteryBoxObject;
    public Vector3 mysteryBoxPosition;
    public List<Transform> allMysteryBoxes = new List<Transform>();

    public static Bundle CurrentBundleData { get; private set; }
    public static string CurrentWord { get; private set; }
    public static Calculation CurrentMathData { get; private set; }

    
    public AudioClip BoxCrashSound;
    public AudioClip DestroyWordSound;

    private AudioClip successSound;
    private AudioClip loseSound;
    private AudioClip tryAgainSound;
    private AudioClip readWordClip;
    private AudioClip calculateItClip;
    private AudioClip firstPuzzleClip;

    private bool isFirstWord = true;
    private bool isFirstCalculation = true;
    private int wordsCounter = 0;

    private string locale;

    private AudioSource audioData;

    public string mysteryBoxName = "mystery_box";

    private EffectsScript effectsProducer;
    private GetDataScript dataSource;
    private StatesScript states;

    public GameObject charBoy;
    private CharAnimationsScript charAnimator;
    // private bool fileDownloaded = false;

    void Start()
    {
        CleanAppCache();
        effectsProducer = this.GetComponent<EffectsScript>();
        dataSource = this.GetComponent<GetDataScript>();
        states = this.GetComponent<StatesScript>();
        charAnimator = charBoy.GetComponent<CharAnimationsScript>();

        audioData = GetComponent<AudioSource>();

        PremiumPopup.SetActive(false);
        Word.SetActive(false);
        MathObject.SetActive(false);
        Preloader.SetActive(false);

        CurrentBundleData = new Bundle() {Name = this.mysteryBoxName };

        locale = ServerClientScript.User != null && ServerClientScript.User.Locale != null && ServerClientScript.User.Locale != string.Empty ?
           ServerClientScript.User.Locale : Application.systemLanguage == SystemLanguage.Russian ?
           "ru-RU" : "en-US";

        successSound = SoundsSource.Instance.GetClip("success", locale);
        loseSound = SoundsSource.Instance.GetClip("wrong", locale);
        tryAgainSound = SoundsSource.Instance.GetClip("oops", locale);
        readWordClip = SoundsSource.Instance.GetClip("read", locale);
        calculateItClip = SoundsSource.Instance.GetClip("calculate", locale); 
        firstPuzzleClip = SoundsSource.Instance.GetClip("first_puzzle", locale); 

        for (int i = 0; i < 15; i++)
            CreateMysteryBox();
    }


    public void CreateMysteryBox()
    {
        Debug.Log("CreateMysteryBox");

        int signX = UnityEngine.Random.value < 0.5 ? 1 : -1;
        float x = UnityEngine.Random.value * 2.5f * signX; ;
        int signY = UnityEngine.Random.value < 0.5 ? 1 : -1;
        float y = -0.5f; //UnityEngine.Random.value * signY;
        int signZ = UnityEngine.Random.value < 0.5 ? 1 : -1;
        float z = UnityEngine.Random.value * 2.5f * signZ;

        Transform objectTransform = Instantiate(mysteryBoxPrefab, new Vector3(x, y, z), Quaternion.identity, mysteryBoxParent.transform) as Transform;
        objectTransform.localPosition = new Vector3(x, y, z);
        objectTransform.gameObject.name = mysteryBoxName;
        objectTransform.gameObject.AddComponent<BoxCollider>();

        allMysteryBoxes.Add(objectTransform);

        Debug.Log("MYSTERY BOX CREATED");
    }

    private void ShowMath()
    {
        for (int j = 0; j < MathObject.transform.childCount; ++j)
        {
            Transform currentItem = MathObject.transform.GetChild(j);
            //Search by name
            if (currentItem.name.Equals("txt_expression"))
            {
                currentItem.GetComponent<Text>().text = $"{CurrentMathData.FirstPart} {CurrentMathData.Sign} {CurrentMathData.SecondPart} = ";
            }
        }
        MathObject.SetActive(true);
        if(isFirstCalculation && StartGameScript.GamesCounter == 1)
        {
            isFirstCalculation = false;
            audioData.Stop();
            audioData.PlayOneShot(calculateItClip, 0.7F);
        }
    }


    public static void CleanAppCache()
    {
        if (Caching.ClearCache())
        {
            Debug.Log("Successfully cleaned the cache.");
        }
        else
        {
            Debug.Log("Cache is being used.");
        }
    }

    public void ShowTaskDestroy()
    {
        if (effectObject != null)
            Destroy(effectObject);
        audioData.PlayOneShot(DestroyWordSound, 0.7F);
        if(Word.activeInHierarchy)
            effectObject = effectsProducer.ShowLoseEffect(Word.transform.parent, Word.transform.position);
        else if(MathObject.activeInHierarchy)
            effectObject = effectsProducer.ShowLoseEffect(MathObject.transform.parent, MathObject.transform.position);
    }

    private void Update()
    {
        //updating score
        this.ScoreObj.GetComponent<Text>().text = string.Format("{0}", StatesScript.Score);

        //hiding unused elements
        if (mysteryBoxObject != null && StatesScript.State != GameState.mystery)
        {
            mysteryBoxObject.gameObject.SetActive(false);
        }
        if (StatesScript.State != GameState.text)
        {
            Word.SetActive(false);
        }
        else
        {
            Word.SetActive(true);
        }
        if (StatesScript.State != GameState.math)
        {
            MathObject.SetActive(false);
        }
        else
        {
            MathObject.SetActive(true);
        }
        if (StatesScript.State != GameState.model && StatesScript.State != GameState.text && model3DObject != null)
        {
            model3DObject.Destroy();
            model3DObject = null;
        }

        //reading success
        if (CurrentBundleData != null && StatesScript.State == GameState.model && ModelHasToBeLoaded)
        {
            StatesScript.CurrentResult = false;
            ModelHasToBeLoaded = false;
            //audioData.PlayOneShot(SuccessSound, 0.7F);// play success sound
            charAnimator.Happy(); //char's reaction
            if (CurrentBundleData.IsAction)
            {
                charAnimator.HandleAction(CurrentBundleData.Name);
            }
            else
            {
                model3DObject.Display = true;                                       
            }

            //show in-app purchase popup every 7 words
            if(!ServerClientScript.User.IsPremium && wordsCounter%7 == 0)
            {
                PremiumPopup.SetActive(true);
            }
        }
        //math success
        else if(StatesScript.CurrentResult == true)
        {
            StatesScript.CurrentResult = false;
           // audioData.PlayOneShot(SuccessSound, 0.7F);// play success sound
            charAnimator.Happy(); //char's reaction
            effectObject = effectsProducer.ShowSuccessEffect(modelsParent.transform, mysteryBoxPosition);
            charAnimator.PointOnModel();
        }

        //spinning object around:
        for (int i = 0; i < allMysteryBoxes.Count; i++)
        {
            allMysteryBoxes[i].transform.Rotate(Vector3.up, 15f * Time.deltaTime);
        }
    }

    public void OnMysteryBoxClicked(GameObject targetObject)
    {
        if (StatesScript.State == GameState.model)
        {
            Preloader.SetActive(false);
            Destroy(effectObject);
            if(model3DObject != null)
                model3DObject.Destroy();

            for (int i = 0; i < allMysteryBoxes.Count; i++)
            {
                allMysteryBoxes[i].gameObject.SetActive(true);
                StatesScript.State = GameState.mystery;
            }
        }
        if (StatesScript.State == GameState.mystery)
        {
            if (this.effectObject != null)
                Destroy(this.effectObject);
            //visual effect
            this.effectObject = effectsProducer.ShowHideMysteryBoxEffect(mysteryBoxParent.transform, targetObject.transform.position);
            //sound effect
            audioData.PlayOneShot(BoxCrashSound, 0.7F);
            mysteryBoxObject = targetObject.transform;
            mysteryBoxPosition = targetObject.transform.position;
            //if (bundleIndex >= bundles.Count)
            //bundleIndex = 0;

            int rand = new System.Random().Next(0, 10); 
            if (rand < 6) //word mode
            {
                CurrentBundleData = dataSource.GetNextBundle();
                targetObject.SetActive(false);
                Word.name = CurrentBundleData.Name;
            
                foreach (Word w in CurrentBundleData.Words)
                {
                    if (w.Locale == locale)
                    {
                        CurrentWord = w.Text;
                        break;
                    }
                }
                Word.GetComponent<TextMesh>().text = CurrentWord;
                Word.SetActive(true);
                if (isFirstWord && StartGameScript.GamesCounter == 1)
                {
                    isFirstWord = false;
                    audioData.Stop();
                    audioData.PlayOneShot(readWordClip, 0.7F);
                }
                StatesScript.State = GameState.text;
                if (!CurrentBundleData.IsAction)
                {
                    model3DObject = new ModelObject(this, modelsParent, mysteryBoxObject.transform.position,
                       CurrentBundleData, effectsProducer, audioData, tryAgainSound, Preloader,
                       isIos, effectObject, charAnimator);
                }
                wordsCounter++;

            }
            else //math mode
            {
                CurrentMathData = dataSource.GetNextMath();
                StatesScript.State = GameState.math;
                ShowMath();
            }
            StatesScript.TriesCounter = 0; //set to 0 for a new task
            charAnimator.Thoughtfull(); //char's reaction

        }
    }

    public void OnModelClicked(GameObject targetObject)
    {
        targetObject.SetActive(false);
        Preloader.SetActive(false);
        StatesScript.State = GameState.mystery;
        mysteryBoxObject.gameObject.SetActive(true);
    }
}
