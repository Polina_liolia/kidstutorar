﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingsScript : MonoBehaviour
{
    public bool playOnSave = false;
    public InputField UserNameInput;

    public Button BtnComplexity1;
    public Button BtnComplexity2;
    public Button BtnComplexity3;

    public Button BtnSave;
    public Button BtnAccauntData;

    public Dropdown languageSelect;

    private string userName = "Polina";
    private int complexity = 0;
    private string language = string.Empty;


    private void Start()
    {
        language = ServerClientScript.User != null && ServerClientScript.User.Locale != null && ServerClientScript.User.Locale != string.Empty ?
           ServerClientScript.User.Locale : Application.systemLanguage == SystemLanguage.Russian ?
           "ru-RU" : "en-US";
        languageSelect.value = language.Equals("ru-RU") ? 0 : 1;

        if (ServerClientScript.User != null)
        {
            userName = ServerClientScript.User.Name;
            UserNameInput.text = userName != null ? userName : string.Empty ;

            complexity = ServerClientScript.User.ComplexityLevel;
            ChangeComplexity(complexity);
        }

        if (playOnSave) //settings just before start to play
        {
            BtnAccauntData.gameObject.SetActive(false);
            BtnSave.GetComponentInChildren<Text>().text = language.Equals("ru-RU") ? "Играть!" : "Play!";
        }
    }

    public void OnNameInputEditEnd(InputField input)
    {
        userName = input.text;
    }

    public void ChangeComplexity(int complexity)
    {
        this.complexity = complexity;
        setComplexityDisabled(complexity);
    }

    private void setAllComplexityEnabled()
    {
        BtnComplexity1.interactable = true;
        BtnComplexity2.interactable = true;
        BtnComplexity3.interactable = true;
    }

    public void setComplexityDisabled(int complexity)
    {
        setAllComplexityEnabled();
        switch (complexity)
        {
            case 1:
                BtnComplexity1.interactable = false;
                break;
            case 2:
                BtnComplexity2.interactable = false;
                break;
            case 3:
                BtnComplexity3.interactable = false;
                break;
        }
    }

    public void OnSave()
    {
        bool isNameChanged = true,
            isComplexityChanged = true,
            isLanguageChanged = true;

        userName = UserNameInput.text;
        complexity = !BtnComplexity2.interactable ? 2 : !BtnComplexity3.interactable ? 3 : 1;
        string newLanguage = languageSelect.value == 0 ? "ru-RU" : "en-US";

        if (ServerClientScript.IsSignedIn)
        {
            ServerClientScript.User = ServerClientScript.User != null ? ServerClientScript.User : GameDataController.User;
            //checking what data was changed 
            if (userName.Equals(ServerClientScript.User.Name))
            {
                isNameChanged = false;
            }

            if (complexity.Equals(ServerClientScript.User.ComplexityLevel))
            {
                isComplexityChanged = false;
            }

            if (newLanguage.Equals(language))
            {
                isLanguageChanged = false;
            }

            //changing data on server
            if (isNameChanged)
            {
                ServerClientScript.User.Name = userName;
                if (ServerClientScript.IsSignedIn)
                {
                    StartCoroutine(ServerClientScript.SetName(userName));
                }
                else
                {
                    GameDataController.User = ServerClientScript.User;
                }
            }

            if (isComplexityChanged)
            {
                ServerClientScript.User.ComplexityLevel = complexity;
                if (ServerClientScript.IsSignedIn)
                {
                    StartCoroutine(ServerClientScript.SetComplexity(complexity));
                }
                else
                {
                    GameDataController.User = ServerClientScript.User;
                }
            }

            if (isLanguageChanged)
            {
                if (ServerClientScript.User == null)
                {
                    ServerClientScript.User = GameDataController.User;
                }
                ServerClientScript.User.Locale = newLanguage;
                if (ServerClientScript.IsSignedIn)
                {
                    StartCoroutine(ServerClientScript.SetLocale(newLanguage));
                }
                else
                {
                    GameDataController.User = ServerClientScript.User;
                }
            }
        }
        else
        {
            ServerClientScript.User = GameDataController.User;
            ServerClientScript.User.ComplexityLevel = complexity;
            ServerClientScript.User.Name = userName;
            ServerClientScript.User.Locale = newLanguage;
        }

        //updating data in file
        GameDataController.User = ServerClientScript.User;

        if (playOnSave)
            SceneManager.LoadScene(sceneName: "MainScene");
        else
            SceneManager.LoadScene(sceneName: "MenuScene");
    }
}
