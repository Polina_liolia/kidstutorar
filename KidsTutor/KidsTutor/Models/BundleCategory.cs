﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KidsTutor.Models
{
    public class BundleCategory
    {
        public int Id { get; set; }
        public int BundleId { get; set; }
        public Bundle Bundle { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public BundleCategory()
        {

        }

        public BundleCategory(int bundleId, int categoryId)
        {
            BundleId = bundleId;
            CategoryId = categoryId;
        }
    }
}
