namespace KidsTutorAPI.Migrations
{
    using KidsTutorAPI.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;

    internal sealed class Configuration : DbMigrationsConfiguration<KidsTutorAPI.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(KidsTutorAPI.Models.ApplicationDbContext context)
        {
            context.Database.ExecuteSqlCommand("delete from dbo.AspNetUsers");
            context.Database.ExecuteSqlCommand("delete from dbo.AspNetUserClaims");
            context.Database.ExecuteSqlCommand("delete from dbo.AspNetUserLogins");
            context.Database.ExecuteSqlCommand("delete from dbo.AspNetUserRoles");
            context.Database.ExecuteSqlCommand("delete from dbo.AspNetRoles");


            //CATEGORIES
            context.Database.ExecuteSqlCommand("delete from dbo.Categories");

            Category natureCategory = new Category("Nature");
            context.Categories.AddOrUpdate(natureCategory);
            Category foodCategory = new Category("Food");
            context.Categories.AddOrUpdate(foodCategory);
            Category transportCategory = new Category("Transport");
            context.Categories.AddOrUpdate(transportCategory);
            Category otherCategory = new Category("Other");
            context.Categories.AddOrUpdate(otherCategory);
            Category successSoundsCategory = new Category("SuccessSound");
            context.Categories.AddOrUpdate(successSoundsCategory);
            Category loseSoundsCategory = new Category("LoseSound");
            context.Categories.AddOrUpdate(loseSoundsCategory);
            Category mysteryBoxCategory = new Category("MysteryBox");
            context.Categories.AddOrUpdate(mysteryBoxCategory);
            Category actionsCategory = new Category("Actions");
            context.Categories.AddOrUpdate(actionsCategory);

            //BUNDLES
            context.Database.ExecuteSqlCommand("delete from dbo.Bundles");

            string ftpRoot3D = @"";
            Bundle bundle = null;

            bundle = new Bundle("rose", String.Format("{0}{1}", ftpRoot3D, "rose"), "rose");
            bundle.Categories.Add(natureCategory);
            bundle.Words.Add(new Word("����", "ru-RU", 1));
            bundle.Words.Add(new Word("rose", "en-US", 1));
            context.Bundles.AddOrUpdate(bundle);

            bundle = new Bundle("tree", String.Format("{0}{1}", ftpRoot3D, "tree"), "Tree_FBX");
            bundle.Categories.Add(natureCategory);
            bundle.Words.Add(new Word("������", "ru-RU", 2));
            bundle.Words.Add(new Word("tree", "en-US", 1));
            context.Bundles.AddOrUpdate(bundle);

            //transport
            bundle = new Bundle("boat", String.Format("{0}{1}", ftpRoot3D, "boat"), "OldBoat");
            bundle.Categories.Add(transportCategory);
            context.Bundles.AddOrUpdate(bundle);
            bundle.Words.Add(new Word("�����", "ru-RU", 1));
            bundle.Words.Add(new Word("boat", "en-US", 1));

            //other
            bundle = new Bundle("basketball", String.Format("{0}{1}", ftpRoot3D, "basketball"), "basketball");
            bundle.Categories.Add(otherCategory);
            bundle.Words.Add(new Word("���", "ru-RU", 1));
            bundle.Words.Add(new Word("ball", "en-US", 1));
            context.Bundles.AddOrUpdate(bundle);

            bundle = new Bundle("converse", String.Format("{0}{1}", ftpRoot3D, "converse"), "converse_fbx");
            bundle.Categories.Add(otherCategory);
            bundle.Words.Add(new Word("����", "ru-RU", 1));
            bundle.Words.Add(new Word("sneakers", "en-US", 3));
            context.Bundles.AddOrUpdate(bundle);

            bundle = new Bundle("oven", String.Format("{0}{1}", ftpRoot3D, "oven"), "DB_Apps&Tech_04_13");
            bundle.Categories.Add(otherCategory);
            bundle.Words.Add(new Word("����", "ru-RU", 1));
            bundle.Words.Add(new Word("oven", "en-US", 1));
            context.Bundles.AddOrUpdate(bundle);

            bundle = new Bundle("house", String.Format("{0}{1}", ftpRoot3D, "house"), "house_obj");
            bundle.Categories.Add(otherCategory);
            bundle.Words.Add(new Word("���", "ru-RU", 1));
            bundle.Words.Add(new Word("house", "en-US", 1));
            context.Bundles.AddOrUpdate(bundle);

            bundle = new Bundle("chest", String.Format("{0}{1}", ftpRoot3D, "chest"), "treasure_chest");
            bundle.Categories.Add(otherCategory);
            bundle.Words.Add(new Word("������", "ru-RU", 2));
            bundle.Words.Add(new Word("chest", "en-US", 1));
            context.Bundles.AddOrUpdate(bundle);
            

            //actions:
            bundle = new Bundle("boxing", "-", "-", true);
            bundle.Categories.Add(actionsCategory);
            bundle.Words.Add(new Word("����", "ru-RU", 1));
            bundle.Words.Add(new Word("boxing", "en-US", 2));
            context.Bundles.AddOrUpdate(bundle);

            bundle = new Bundle("jumping", "-", "-", true);
            bundle.Categories.Add(actionsCategory);
            bundle.Words.Add(new Word("������", "ru-RU", 2));
            bundle.Words.Add(new Word("jump", "en-US", 1));
            context.Bundles.AddOrUpdate(bundle);

            bundle = new Bundle("samba_dancing", "-", "-", true);
            bundle.Categories.Add(actionsCategory);
            bundle.Words.Add(new Word("�����", "ru-RU", 1));
            bundle.Words.Add(new Word("dance", "en-US", 1));
            context.Bundles.AddOrUpdate(bundle);

            bundle = new Bundle("martelo_do_chau", "-", "-", true);
            bundle.Categories.Add(actionsCategory);
            bundle.Words.Add(new Word("����", "ru-RU", 1));
            bundle.Words.Add(new Word("trick", "en-US", 1));
            context.Bundles.AddOrUpdate(bundle);

            context.Database.ExecuteSqlCommand("delete from dbo.Rewards");
            //rewards
            Reward reward = null;
            string ftpRoot2D = @"pics/";

            for(int i = 1; i <= 30; i++)
            {
                reward = new Reward($"reward{i}", $"{ftpRoot2D}_reward_{i}.jpg", i);
                context.Rewards.AddOrUpdate(reward);
            }     
            

            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                List<string> errors = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    errors.Add(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errors.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                    }
                }
                System.IO.File.WriteAllLines(@"Errors.txt", errors.ToArray());
                throw;
            }

        }
    }
}
