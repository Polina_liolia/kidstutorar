﻿using KidsTutorAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KidsTutorAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CalculationsController : ApiController
    {
        [ResponseType(typeof(IList<Calculation>))]
        [Route("api/calculations/{complexity}/{count}")]
        public IHttpActionResult GetCalculations(int complexity, int count)
        {
            List<string> signs = new List<string>() { "+", "-" }; //signs for basic complexity
            if(complexity >= 3)
            {
                signs.AddRange(new List<string>() { ":", "x" }); //extra signs for higher complexity
            }

            int max = complexity == 1 ? 10 :
                complexity == 2 ? 20 : 100;

            List<Calculation> calculations = new List<Calculation>();

            Random rand = new Random();

            for(int i = 0; i < count; i++)
            {
                string sign = signs[rand.Next(signs.Count)];

                int first = 0;
                int second = 0;

                switch (sign)
                {
                    case "+":
                        {
                            first = rand.Next(max + 1);
                            second = rand.Next(max + 1);
                        }
                        break;
                    case "-":
                        {
                            second = rand.Next(max + 1);
                            first = rand.Next(second, max + 1);
                        }
                        break;
                    case "x":
                        {
                            first = rand.Next(11);
                            second = rand.Next(11);
                        }
                        break;
                    case ":":
                        {
                            second = rand.Next(11);
                            first = rand.Next(11) * second;

                        }
                        break;
                }

                calculations.Add(new Calculation(first, second, sign));
            }

            return Ok(new { results = calculations });
        }
    }
}
