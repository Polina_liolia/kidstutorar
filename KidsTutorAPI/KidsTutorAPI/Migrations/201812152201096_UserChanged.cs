namespace KidsTutorAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserChanged : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.CategoryUsers", newName: "CategoryApplicationUsers");
            RenameColumn(table: "dbo.CategoryApplicationUsers", name: "User_Id", newName: "ApplicationUser_Id");
            RenameIndex(table: "dbo.CategoryApplicationUsers", name: "IX_User_Id", newName: "IX_ApplicationUser_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.CategoryApplicationUsers", name: "IX_ApplicationUser_Id", newName: "IX_User_Id");
            RenameColumn(table: "dbo.CategoryApplicationUsers", name: "ApplicationUser_Id", newName: "User_Id");
            RenameTable(name: "dbo.CategoryApplicationUsers", newName: "CategoryUsers");
        }
    }
}
