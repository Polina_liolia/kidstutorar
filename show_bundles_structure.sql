use tutorgamedb;
select count(Id) as Total	--56
from [dbo].[Bundles]
union
select count(Id) as Low		--30
from [dbo].[Bundles] 
where [ComplexityLevel] = 1 
union 
select count(Id) as Medium	--17
from [dbo].[Bundles] 
where [ComplexityLevel] = 2 
union
select count(Id) as High	--9
from [dbo].[Bundles] 
where [ComplexityLevel] = 3; 