﻿using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.RectTransform;

public abstract class FileExtraData
{
    public string Host { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
    public string FilePath { get; set; }
    public string FileName { get; set; }
}

public abstract class AsyncFilesDownloader
{
    public WebClient Client { get; protected set; }
    protected FileExtraData data;
    protected string localPath;

    public AsyncFilesDownloader(FileExtraData data)
    {
        Client = new WebClient();
        this.data = data;
    }

    public void LoadAsync()
    {
        localPath = $"{Application.persistentDataPath}/{data.FileName}";
        //  if (!File.Exists(localPath))
        //{
        Debug.Log($"Download reward file started...");

        Client.Credentials = new NetworkCredential(data.UserName, data.Password);
        Client.DownloadFileCompleted += new AsyncCompletedEventHandler(this.DownloadFileCompleted);
        Client.DownloadFileAsync(new Uri($"{data.Host}/{data.FilePath}"), localPath);
        //}
        //else
        //{
        //    DownloadFileCompleted(null, null);
        //}
    }

    protected abstract void DownloadFileCompleted(object sender, AsyncCompletedEventArgs e);

}


public class RewardsFileExtraData : FileExtraData
{
    public int Count { get; set; }
    public int PartsHidden { get; set; }
    public GameObject Parent { get; set; }
    public Image Puzzle { get; set; }

}

public class RewardsAsyncLoader : AsyncFilesDownloader
{
    public RewardsAsyncLoader(RewardsFileExtraData data) : base(data)
    {

    }


     protected override void DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
     {
         if (data is RewardsFileExtraData)
         {
             RewardsFileExtraData rewardData = data as RewardsFileExtraData;
             if (rewardData != null && File.Exists(localPath))
             {
                //loading texture
                 byte[] fileData;
                 fileData = File.ReadAllBytes(localPath);
                 Texture2D tex = null;//new Texture2D(100, 100, TextureFormat.DXT1, false);
                 tex = new Texture2D(2, 2);
                 tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
                 File.Delete(localPath);
    		    
                //creating sprite from texture
                 Sprite sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, 400, 400), new Vector2(0, 0), 100.0f);

                //Create the GameObject
                GameObject newObj = new GameObject(); 
                 newObj.name = rewardData.FileName;

                //Add the Image Component script
                Image newImage = newObj.AddComponent<Image>();
                newImage.raycastTarget = false; //disable to make view scrollable with touch

                //Assign the newly created Image GameObject as a Child of the Parent Panel.
                newObj.GetComponent<RectTransform>().SetParent(rewardData.Parent.transform);

                //Set the Sprite of the Image Component on the new GameObject
                newImage.sprite = sprite; 
                 newImage.rectTransform.sizeDelta = new Vector2(400, 400);
                 
                 newImage.rectTransform.anchorMax = new Vector2(0, 1);
                 newImage.rectTransform.anchorMin = new Vector2(0, 1);
                 newImage.rectTransform.pivot = new Vector2(0, 1);
                 
                 int newY = - 10 - 200 * (rewardData.Count - 1);
                 newImage.rectTransform.localPosition = new Vector3(22, newY, 0);

                //adding a puzzle
                if(rewardData.Puzzle != null)
                {
                    Image puzzle = MonoBehaviour.Instantiate(rewardData.Puzzle, new Vector3(0, 1, 1), Quaternion.identity);
                    puzzle.transform.SetParent(newObj.transform);
                    puzzle.rectTransform.sizeDelta = new Vector2(400, 400);
                    puzzle.rectTransform.anchorMax = new Vector2(0, 1);
                    puzzle.rectTransform.anchorMin = new Vector2(0, 1);
                    puzzle.rectTransform.pivot = new Vector2(0, 1);
                    puzzle.rectTransform.localPosition = new Vector3(0, 1, 1);
                    puzzle.raycastTarget = false; //disable to make view scrollable with touch
                }

                //Activate the GameObject
                newObj.SetActive(true); 

                //check scroll view height and change if needed
                RectTransform parentRectTransform = rewardData.Parent.GetComponent<RectTransform>();
                float currentScrollViewHeight = parentRectTransform.sizeDelta.y;
                float targetScrollViewHeight = -newY + 210;
                if (currentScrollViewHeight < targetScrollViewHeight)
                {
                    parentRectTransform.SetSizeWithCurrentAnchors(Axis.Vertical, targetScrollViewHeight);
                }

            }
         }
     }
}
