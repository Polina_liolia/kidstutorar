package UserInterface.ApplicationUsers;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import tutorgamedbaccess.ApplicationUser;

public class ApplicationUserTableModel extends AbstractTableModel{

    List<ApplicationUser> applicationUsers;
       
    //columns definition
    String[] columns = new String[]{
       "Id", "User name", "Name in game",  "Complexity", "Score"
    };
    private Class[] columnsTypes = new Class[]{
        String.class, String.class, String.class, Integer.class, Long.class
    };

    public ApplicationUserTableModel(List<ApplicationUser> applicationUsers) {
        this.applicationUsers = applicationUsers;
    }
    
    @Override
    public int getRowCount() {
        return applicationUsers.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

 
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ApplicationUser currentApplicationUser = applicationUsers.get(rowIndex);
        return 0 == columnIndex ? currentApplicationUser.getId(): 
               1 == columnIndex ? currentApplicationUser.getUserName():
               2 == columnIndex ? currentApplicationUser.getName(): 
               3 == columnIndex ? currentApplicationUser.getComplexityLevel():
               4 == columnIndex ? currentApplicationUser.getScore() : null;
    }
    @Override
    public String getColumnName(int column) {
        return columns[column];
    }
    
    @Override
    public Class<?> getColumnClass(int column)
    {
        return columnsTypes[column];
    }

    public ApplicationUser getApplicationUser(int row) {
        return applicationUsers.get(row);
    }
}
