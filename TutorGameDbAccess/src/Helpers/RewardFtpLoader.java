package Helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.apache.commons.net.ftp.FTPClient;

public class RewardFtpLoader {

    JFrame parent;

    public RewardFtpLoader(JFrame parent) {
        this.parent = parent;
    }

    private File chooseFile() {
        File file = null;
        JFileChooser fileChooser = new JFileChooser();
        //fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        int result = fileChooser.showOpenDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            file = fileChooser.getSelectedFile();
        }
        return file;
    }

    public String uploadReward(String ftpPath, String login, String password) {
        String assetName = "";
        FTPClient client = new FTPClient();
        FileInputStream fis = null;

        try {
            client.connect(ftpPath);
            client.login(login, password);

            //choose file
            File file = chooseFile();
            // Create an InputStream of the file to be uploaded
            fis = new FileInputStream(file);
            // Store file to server
            client.storeFile("pics/" + file.getName(), fis);
            assetName = "pics/" + file.getName().replaceFirst("[.][^.]+$", "");
            client.logout();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(parent, "File uploading failed: " + e.getMessage(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                client.disconnect();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(parent, "FTP client disconnecting failed: " + e.getMessage(),
                        "Error", JOptionPane.ERROR_MESSAGE);
            }

        }
        return assetName;
    }
}
