﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;

public class ModelObject {

    private MonoBehaviour monoInstance;
    private GameObject modelsParent;
    private GameObject preloader;
    private bool isIos;
    private Bundle bundleData;

    private GameObject model3DObject;
    private GameObject effectObject;

    private Vector3 position;

    private CharAnimationsScript charAnimator;
    private EffectsScript effectsProducer;
    private AudioSource audioData;
    private AudioClip tryAgainSound;

    private bool isReady = false;

    private bool display = false;
    public bool Display 
    { 
        get { return display; }
        set 
        {
            display = value;
            if (display && isReady)
            {
                DisplayModel();
            }
            else if (display)
            {
                preloader.SetActive(true);
            }
            else
            {
                if(model3DObject != null)
                {
                    model3DObject.SetActive(false);
                }
            }
        }
     }

    private readonly string ftpPath = @"ftp://ftp.drivehq.com/";
    private readonly string userName = "polinaliolia";
    private readonly string password = "drivehqgjkbyf12";
    private readonly string directoryIos = "assets_ios";
    private readonly string dirAndroid = "assets_android";


    public ModelObject(MonoBehaviour monoInstance,  GameObject modelsParent, Vector3 position,
        Bundle bundleData, EffectsScript effectsProducer, 
        AudioSource audioData, AudioClip tryAgainSound, GameObject preloader = null, bool isIos = true, 
        GameObject effectObject = null,  CharAnimationsScript charAnimator = null)
    {
        this.monoInstance = monoInstance;
        this.modelsParent = modelsParent;
        this.preloader = preloader;
        this.isIos = isIos;
        this.bundleData = bundleData;
        this.effectObject = effectObject;
        this.position = position;
        this.charAnimator = charAnimator;
        this.effectsProducer = effectsProducer;
        this.audioData = audioData;
        this.tryAgainSound = tryAgainSound;



        if(!bundleData.IsAction)
            this.monoInstance.StartCoroutine(CreateModel());
    }

    private IEnumerator CreateModel()
    {
        Debug.Log("CreateModel");
        //preloader.SetActive(true);
        if (bundleData.Path.Equals(string.Empty))
        {
            model3DObject = effectsProducer.ShowSuccessEffect(modelsParent.transform, position);
            charAnimator.PointOnModel();
            //preloader.SetActive(false);
            yield return null;
        }
        else
        { 
            DownloadFile();
        }
    }

    private void DownloadFile()
    {
        string file = bundleData.Path;
        string directory = isIos ? directoryIos : dirAndroid;
        if (!File.Exists($"{Application.persistentDataPath}/{file}") && !Caching.IsVersionCached($"file://{Application.persistentDataPath}/{bundleData.Path}", 0))
        {
            Debug.Log($"Download file started...");
            WebClient client = new WebClient();
            client.Credentials = new NetworkCredential(userName, password);
            client.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler(DownloadFileCompleted);
            client.DownloadFileAsync(new Uri($"{ftpPath}/{directory}/{file}"), $"{Application.persistentDataPath}/{file}");
        }
        else
        {
            monoInstance.StartCoroutine(DownloadModelAndCache());
        }
    }

    private void DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        if (e.Error == null)
        {
            // this.fileDownloaded = true;
            Debug.Log($"Download file completed");
            monoInstance.StartCoroutine(DownloadModelAndCache());
        }
        else
        {
            preloader.SetActive(false);
            Debug.Log($"Download file error: {e.Error.Message}");
        }
    }


    private IEnumerator DownloadModelAndCache()
    {
        GameObject targetGameObject = null;

        // Wait for the Caching system to be ready
        yield return new WaitUntil(() => Caching.ready);

        // CleanAppCache();

        Debug.Log("LoadFromCacheOrDownload started...");
        // Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
        using (WWW www = WWW.LoadFromCacheOrDownload($"file://{Application.persistentDataPath}/{bundleData.Path}", 0))
        {
            yield return www;

            if (!string.IsNullOrEmpty(www.error))
            {
                preloader.SetActive(false);
                audioData.PlayOneShot(tryAgainSound, 0.7F);
                throw new Exception("WWW download had an error:" + www.error);
            }
            Debug.Log("LoadFromCacheOrDownload succeeded!!!");

            AssetBundle bundle = www.assetBundle;
            Debug.Log("Bundle created!");

            UnityEngine.Object obj = null;

            AssetBundleRequest request = bundle.LoadAssetAsync(bundleData.AssetName);
            yield return request;

            obj = request.asset;

            if (obj is GameObject)
            {
                targetGameObject = obj as GameObject;
                if (targetGameObject != null)
                {
                    model3DObject = targetGameObject;
                    Debug.Log("Asset is a game object!");

                    model3DObject.transform.localScale = new Vector3(0.3F, 0.3F, 0.3F);
                    model3DObject = MonoBehaviour.Instantiate(model3DObject);
                    model3DObject.name = bundleData.Name;
                    model3DObject.layer = 12; //models' layer

                    //add a light component
                    Light lightComp = model3DObject.AddComponent<Light>();
                    //set color and position
                    lightComp.color = Color.white;
                    lightComp.transform.position = new Vector3(8.68f, 7, 48.74f);
                    lightComp.transform.rotation.Set(70, 110, 20, 0);
                    lightComp.cullingMask = 12; //affects only on models' layer


                    model3DObject.transform.SetParent(modelsParent.transform);
                    model3DObject.transform.position = position;
                    model3DObject.SetActive(false);

                    isReady = true;

                    if (Display)
                    {
                        preloader.SetActive(false);
                        DisplayModel();
                    }


                    //Returns BoxCollider
                    var boxCollider = targetGameObject.AddComponent<BoxCollider>();
                    Debug.Log("Object instanciated (targetGameObject)!");
                }
                else
                {
                    Debug.Log("Target game object is null!");
                }

                // Unload the AssetBundles compressed contents to conserve memory
                yield return new WaitForSeconds(3);

                bundle.Unload(false);
                if (bundleData.Name != "mystery_box")
                {
                    File.Delete($"{Application.persistentDataPath}/{bundleData.Path}");
                    Debug.Log("File deleted localy.");
                }
            }

            // memory is freed from the web stream (www.Dispose() gets called implicit
        }
        preloader.SetActive(false);
    }

    private void DisplayModel()
    {
        if (model3DObject == null)
            return;
        if (effectObject != null)
            MonoBehaviour.Destroy(this.effectObject);
        effectObject = effectsProducer.ShowPreModelEffect(modelsParent.transform, model3DObject.transform.position);
        preloader.SetActive(false);
        model3DObject.SetActive(true);
        Debug.Log("MODEL CREATED");
        charAnimator.PointOnModel();
    }

    public void Destroy()
    {
        if (model3DObject != null)
        {
            model3DObject.SetActive(false);
            MonoBehaviour.Destroy(model3DObject);
            model3DObject = null;
        }
    }

}
